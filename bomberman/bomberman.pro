#-------------------------------------------------
#
# Project created by QtCreator 2016-01-24T23:02:09
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bomberman
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bonus.cpp \
    boutontouche.cpp \
    elementmap.cpp \
    mur.cpp \
    murincassable.cpp \
    personnage.cpp \
    sol.cpp

HEADERS  += mainwindow.h \
    bonus.h \
    boutontouche.h \
    coord.h \
    elementmap.h \
    mur.h \
    murincassable.h \
    personnage.h \
    sol.h \
    main.h

FORMS    += mainwindow.ui
