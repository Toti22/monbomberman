#ifndef BOUTONTOUCHE_H
#define BOUTONTOUCHE_H
#include <QWidget>
#include <QObject>
#include <QPushButton>
#include <QAbstractButton>
#include <qabstractbutton.h>

QT_BEGIN_HEADER
QT_BEGIN_NAMESPACE
QT_MODULE(Gui)
class QPushButtonPrivate;
class QMenu;
class MonMenu;
class QStyleOptionButton;
class BoutonTouche : public QPushButton
{
    Q_OBJECT
public:
    BoutonTouche(MonMenu *fenetre, int);
    static int nbBouton;

signals:
    void toucheChoisie(int touche,int joueur,int numBouton);

protected:
    int numBouton;
    int joueur;
    void click();
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void keyPressEvent(QKeyEvent *key);
    virtual bool event(QEvent *eventTemp);

    MonMenu *maFenetre;

};

#endif // BOUTONTOUCHE_H
