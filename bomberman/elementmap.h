#ifndef ELEMENTMAP_H
#define ELEMENTMAP_H
#include <QWidget>
#include "coord.h"
#include <QLabel>
#include <QTimer>


class MonMenu;
class QPixmap;
namespace Ui {
class QWidget;

}
class ElementMap : public QWidget
{
    friend class MonMenu;
    friend class Personnage;
    friend class Bombe;
    friend class Bonus;
    Q_OBJECT

public:
    ElementMap(MonMenu *fenetre);
    ~ElementMap();
    void setPosition(int,int);
    void setTaille(int,int);
    void miseAJour();
    void setImage(QString chemin);

protected:
    int type;
    QLabel *image;
    QPixmap *pixmapImage;
    Coord taille;
    Coord position;
    bool franchissable;
    bool sensibleFlamme;
    bool arreteFlamme;
    MonMenu *maFenetre;

};

class ElementMapSensibleFlamme : public ElementMap
{
    Q_OBJECT
public:
    ElementMapSensibleFlamme(MonMenu*);
    virtual ~ElementMapSensibleFlamme();
    static int nbObjet;
    int numeroObjet;
protected:

private slots:
    virtual void destructionParFlamme(int)=0;

};

class Bombe : public ElementMapSensibleFlamme
{
    friend class MonMenu;
    friend class Personnage;
    friend class Bonus;
    Q_OBJECT
public:
    Bombe(MonMenu*, int, short int flammeTemp);//type de bombe
    ~Bombe();
private:

    short int type;
    short int joueur;
    short int flamme;
    bool deplacementBas;
    bool deplacementHaut;
    bool deplacementGauche;
    bool deplacementDroite;
    int compteurAvantBombeExplose;
    QTimer *timer;
    QTimer *timerDeplacement;
    void destructeur();
    bool destructionRetardementVerification;


signals:

    void bombeExplose(Coord,short int,short int,short int);

private slots:
    void explosionBombe();
    virtual void destructionParFlamme(int);
    void destructionRetardement();
    void deplacementBombe();
};

class Flamme : public ElementMap
{
    friend class MonMenu;
    Q_OBJECT
public:
    Flamme(MonMenu*);
    ~Flamme();
    void setDirectionFlamme(QString direction){directionFlamme=direction;}
protected:
    int numeroFlamme;
    QString directionFlamme;
    int compteur;//sert pour anomer la flamme
    QTimer *timer;

protected slots:
    void annimationFlamme();
};

#endif // ELEMENTMAP_H
