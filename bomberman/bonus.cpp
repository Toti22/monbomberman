#include "bonus.h"
#include <QObject>
#include "personnage.h"
#include "mainwindow.h"
#include "coord.h"
#include "elementmap.h"

int Bonus::typeMax=20;

Bonus::Bonus(MonMenu* fenetre, int typeTemp,Coord position):ElementMapSensibleFlamme(fenetre),compteurMortBonus(2)

{
    if (!position.x==0 && !position.y==0)
    {

    bonusAttrape=false;
    type=typeTemp;
    timer = new QTimer();
    timer->setInterval(50);
    timer->start();
    connect(timer,SIGNAL(timeout()),this,SLOT(attrapeParJoueur()));
    setTaille(maFenetre->tailleBlockX,maFenetre->tailleBlockY);
    setPosition(position.x,position.y);
    if (type>=0 && type<=5)setImage("../bonus/bombe.png");
    if (type>5 && type<=10)setImage("../bonus/flamme.png");
    if (type>10 && type<=15)setImage("../bonus/vitesse.png");
    if (type>15 && type<=17)setImage("../bonus/bombeRapide.png");
    if (type>17 && type<=18)setImage("../bonus/bombeLente.png");
    if (type>18 && type<=20)setImage("../bonus/coupDePied.png");
    miseAJour();
    }
}

void Bonus::attrapeParJoueur()
{

    if(bonusAttrape==false)
    {
        for(int i = 0; i<maFenetre->listPersonnage.length();i++)
        {
            if ((maFenetre->listPersonnage[i]->position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX==position.x &&
                    ((maFenetre->listPersonnage[i]->position.y+maFenetre->tailleBlockY/2+1)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY==position.y)
            {
                if (type>=0 && type<=5 && maFenetre->listPersonnage[i]->bombe<8)maFenetre->listPersonnage[i]->bombe++;
                if (type>5 && type<=10 && maFenetre->listPersonnage[i]->flamme<8)maFenetre->listPersonnage[i]->flamme++;
                if (type>10 && type<=15 && maFenetre->listPersonnage[i]->vitesse<8)maFenetre->listPersonnage[i]->vitesse++;
                if (type>15 && type<=17 )
                {
                    maFenetre->listPersonnage[i]->typeBombe=3;
                }
                if (type>17 && type<=18 )
                {
                    maFenetre->listPersonnage[i]->typeBombe=4;
                }
                if (type>18 && type<=20 )
                {
                    maFenetre->listPersonnage[i]->pousser=true;
                }
                setPosition(0,0);
                setImage("../");
                miseAJour();
                bonusAttrape=true;
            }
            for(int i = 0; i<maFenetre->listBombe.length();i++)
            {
                if ((maFenetre->listBombe[i]->position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX==position.x &&
                        ((maFenetre->listBombe[i]->position.y)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY==position.y &&
                        bonusAttrape==false)
                {
                    int indexJoueur=maFenetre->listBombe[i]->joueur-1;
                    if (type>=0 && type<=5 && maFenetre->listPersonnage[indexJoueur]->bombe<8)maFenetre->listPersonnage[indexJoueur]->bombe++;
                    if (type>5 && type<=10 && maFenetre->listPersonnage[indexJoueur]->flamme<8)maFenetre->listPersonnage[indexJoueur]->flamme++;
                    if (type>10 && type<=15 && maFenetre->listPersonnage[indexJoueur]->vitesse<8)maFenetre->listPersonnage[indexJoueur]->vitesse++;
                    if (type>15 && type<=17 )maFenetre->listPersonnage[indexJoueur]->typeBombe=3;
                    if (type>17 && type<=18 )maFenetre->listPersonnage[indexJoueur]->typeBombe=4;
                    if (type>18 && type<=20 )maFenetre->listPersonnage[indexJoueur]->pousser=true;
                    setPosition(0,0);
                    setImage("../");
                    miseAJour();
                    bonusAttrape=true;
                }
            }

        }

    }

}

void Bonus::destructionParFlamme(int numeroElement)
{
    compteurMortBonus--;
    if(numeroElement==numeroObjet && compteurMortBonus<=0)
    {
        setPosition(0,0);
        setImage("../");
        miseAJour();
        bonusAttrape = true;
    }
}
