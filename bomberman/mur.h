#ifndef MUR_H
#define MUR_H
#include "elementmap.h"

class MonMenu;
namespace Ui {
class QWidget;

}
class Mur : public ElementMapSensibleFlamme
{
    Q_OBJECT
public:

    Mur(MonMenu*);
    virtual ~Mur();
private:
    void destruction();
    int testBaba;
    QTimer *timer;
    bool dejaConnecte;

private slots:
    virtual void destructionParFlamme(int);
    void destrutctionRetardement();
};

#endif // MUR_H
