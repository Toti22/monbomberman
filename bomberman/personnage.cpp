#include "personnage.h"
#include "elementmap.h"
#include "mainwindow.h"
#include "coord.h"
#include <QMainWindow>
#include <QPicture>
#include <QWidget>
#include <QEvent>
#include <QKeyEvent>
#include <QSignalTransition>

int Personnage::nbToucheUtile=6;

Personnage::Personnage(MonMenu* fenetre,int numJoueur) : ElementMapSensibleFlamme(fenetre),enVie(true),joueur(numJoueur)
{
    bombe=2;
    flamme=2;
    vitesse=2;
    typeBombe=1;
    lancer=false;
    pousser=true;
    position.x=1;
    position.y=1;
    deplacementBas=false;
    deplacementDroite=false;
    deplacementHaut=false;
    deplacementGauche=false;
    arreteFlamme=false;

    franchissable=true;

    timer = new QTimer();
    timer->setInterval(50-vitesse*4);
    timer->start();
    connect(timer,SIGNAL(timeout()),this,SLOT(deplacerJoueur()));
    connect(maFenetre,SIGNAL(poserBombeSignal(Personnage*)),this,SLOT(poserBombe(Personnage*)));
    if(joueur==1)
    {
        toucheBas = Qt::Key_Down;
        toucheHaut = Qt::Key_Up;
        toucheGauche = Qt::Key_Left;
        toucheDroite = Qt::Key_Right;
        touchePoserBombe = Qt::Key_Shift;
        toucheAction1 = Qt::Key_N;
    }
    if(joueur==2)
    {
        toucheBas = Qt::Key_S;
        toucheHaut = Qt::Key_Z;
        toucheGauche = Qt::Key_Q;
        toucheDroite = Qt::Key_D;
        touchePoserBombe = Qt::Key_A;
        toucheAction1 = Qt::Key_E;
    }
    if(joueur==3)
    {
        toucheBas = Qt::Key_H;
        toucheHaut = Qt::Key_Y;
        toucheGauche = Qt::Key_G;
        toucheDroite = Qt::Key_J;
        touchePoserBombe = Qt::Key_T;
        toucheAction1 = Qt::Key_U;
    }
    if(joueur==4)
    {
        toucheBas = Qt::Key_L;
        toucheHaut = Qt::Key_O;
        toucheGauche = Qt::Key_K;
        toucheDroite = Qt::Key_M;
        touchePoserBombe = Qt::Key_I;
        toucheAction1 = Qt::Key_P;
    }
}

Personnage::Personnage(MonMenu* fenetre,int joueur,Coord p) : ElementMapSensibleFlamme(fenetre)
{
    bombe=1;
    flamme=1;
    vitesse=1;
    lancer=false;
    pousser=false;
    position.x=p.x;
    position.y=p.y;
    deplacementBas=false;
    deplacementDroite=false;
    deplacementHaut=false;
    deplacementGauche=false;
}

Personnage::Personnage(MonMenu* fenetre,int joueur,int b,int f,int v,Coord p) : ElementMapSensibleFlamme(fenetre)
{
    bombe=b;
    flamme=f;
    vitesse=v;
    lancer=false;
    pousser=false;
    position.x=p.x;
    position.y=p.y;
    deplacementBas=false;
    deplacementDroite=false;
    deplacementHaut=false;
    deplacementGauche=false;
}

Personnage::~Personnage()
{

}

void Personnage::destructionParFlamme(int numeroElement)
{
    if(numeroElement==numeroObjet)
    {
        QString chemin="../personnage/personnage";
        chemin += joueur+0x30;
        chemin+='/';
        chemin+="mort4.png";
        setImage(chemin);
        miseAJour();
        enVie=false;
        //delete this;
    }
}

void Personnage::deplacerJoueur()
{
    if (enVie==true)
    {
        QString chemin;
        chemin="../personnage/personnage";
        chemin+=joueur+0x30;
        chemin+='/';
        //ke = static_cast<QKeyEvent *>(evenement);
        timer->setInterval(60-vitesse*4);

        if (deplacementBas==true)
        {

            compteur++;
            if (compteur>=1 && compteur<6)chemin+="marcheBas1.png";
            else if (compteur>=6 && compteur<10)chemin+="bas.png";
            else if (compteur>=10 && compteur<16)chemin+="marcheBas2.png";
            else if (compteur>=16 && compteur<19)chemin+="bas.png";
            else {chemin+="bas.png";compteur=0;}
            setImage(chemin);
            int deplacementJoueur=position.y+(maFenetre->resolutionTaille.y/100);
            for (int i=0;i<maFenetre->listElementMap.length();i++)
            {

                if(
                        maFenetre->listElementMap[i]->franchissable==false &&

                        (
                        (position.x>=maFenetre->listElementMap[i]->position.x &&
                        position.x<=maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX) ||
                        (
                        position.x+taille.x-taille.x/5>maFenetre->listElementMap[i]->position.x &&
                        position.x+taille.x<maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX)
                        /*||
                        (
                        position.x-tailleBlockXPerso>maFenetre->listElementMap[i]->position.x &&
                        position.x-tailleBlockXPerso<maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX)
                        */)&&
                        position.y+taille.y<maFenetre->listElementMap[i]->position.y &&// position.y+tailleBlockY car on vérifie par rapport à l'enplacement des pieds
                        position.y+taille.y>maFenetre->listElementMap[i]->position.y-maFenetre->tailleBlockY
                        )
                {
                    if (deplacementJoueur+taille.y>maFenetre->listElementMap[i]->position.y)
                    {
                        if(pousser==true)
                        {
                            Bombe *bombe=dynamic_cast<Bombe *>(maFenetre->listElementMap[i]);
                            if ( bombe!=0)
                            {
                                qDebug() << "Bombe devant";
                                bombe->deplacementBas=true;
                                bombe->deplacementDroite=false;
                                bombe->deplacementHaut=false;
                                bombe->deplacementGauche=false;
                            }
                        }
                        deplacementJoueur=position.y+(maFenetre->listElementMap[i]->position.y-(position.y+taille.y+taille.y))+taille.y-2;
                        Coord positionJoueur ;
                        positionJoueur.x=(position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX;
                        positionJoueur.y=int((position.y)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY;
                        for (int i  = 0; i<maFenetre->listElementMap.length();i++)
                        {
                            if(maFenetre->listElementMap[i]->position.y==positionJoueur.y+maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.x <= position.x && maFenetre->listElementMap[i]->position.x +maFenetre->tailleBlockX >= position.x)
                            {
                                bool caseOK=true;
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.x=position.x-(maFenetre->resolutionTaille.x/100);
                                }
                            }
                            if(maFenetre->listElementMap[i]->position.y==positionJoueur.y+maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.x <= position.x+maFenetre->tailleBlockX/1.5 && maFenetre->listElementMap[i]->position.x  >= position.x)
                            {
                                bool caseOK=true;
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.x=position.x+(maFenetre->resolutionTaille.x/100);
                                }
                            }

                        }
                    }
                }
            }
            setPosition(position.x,deplacementJoueur);
            miseAJour();
        }
        else if (deplacementHaut==true)
        {
            compteur++;
            if (compteur>=1 && compteur<6)chemin+="marcheHaut1.png";
            else if (compteur>=6 && compteur<10)chemin+="haut.png";
            else if (compteur>=10 && compteur<16){chemin+="marcheHaut2.png";}
            else if (compteur>=16 && compteur<19)chemin+="haut.png";
            else {chemin+="haut.png";compteur=0;}
            setImage(chemin);
            int deplacementJoueur=position.y-(maFenetre->resolutionTaille.y/100);
            for (int i=0;i<maFenetre->listElementMap.length();i++)
            {

                if(
                        maFenetre->listElementMap[i]->franchissable==false &&

                        (
                            (position.x>=maFenetre->listElementMap[i]->position.x &&
                            position.x<=maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX) ||
                            (
                            position.x+taille.x-taille.x/5>maFenetre->listElementMap[i]->position.x &&
                            position.x+taille.x<maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX)
                        /*||
                        (
                        position.x-maFenetre->tailleBlockX-maFenetre->tailleBlockX/3>=maFenetre->listElementMap[i]->position.x &&
                        position.x-maFenetre->tailleBlockX+maFenetre->tailleBlockX/3<=maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX)
                        */)&&
                        position.y<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY &&
                        position.y-taille.y/2>=maFenetre->listElementMap[i]->position.y
                        )
                {
                    if (deplacementJoueur-taille.y-(maFenetre->tailleBlockY-taille.y)<maFenetre->listElementMap[i]->position.y)
                    {
                        if(pousser==true)
                        {
                            Bombe *bombe=dynamic_cast<Bombe *>(maFenetre->listElementMap[i]);
                            if ( bombe!=0)
                            {
                                qDebug() << "Bombe devant";
                                bombe->deplacementHaut=true;
                                bombe->deplacementDroite=false;
                                bombe->deplacementBas=false;
                                bombe->deplacementGauche=false;
                            }
                        }
                        Coord positionJoueur ;
                        positionJoueur.x=(position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX;
                        positionJoueur.y=int((position.y)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY+maFenetre->tailleBlockY;
                        deplacementJoueur=positionJoueur.y-1;

                        for (int i  = 0; i<maFenetre->listElementMap.length();i++)
                        {
                            if(maFenetre->listElementMap[i]->position.y==positionJoueur.y-maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.x == positionJoueur.x)
                            {
                                bool caseOK=true;
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.x=position.x-(maFenetre->resolutionTaille.x/100);
                                }
                            }
                            if(maFenetre->listElementMap[i]->position.y==positionJoueur.y-maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.x <= position.x+maFenetre->tailleBlockX/1.5 && maFenetre->listElementMap[i]->position.x  >= position.x)
                            {
                                bool caseOK=true;
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.x=position.x+(maFenetre->resolutionTaille.x/100);
                                }
                            }

                        }
                    }
                }
            }
            setPosition(position.x,deplacementJoueur);
            miseAJour();
        }
        else if (deplacementDroite==true)
        {
            compteur++;
            if (compteur>=1 && compteur<6)chemin+="marcheDroite1.png";
            else if (compteur>=6 && compteur<10)chemin+="droite.png";
            else if (compteur>=10 && compteur<16){chemin+="marcheDroite2.png";}
            else if (compteur>=16 && compteur<19)chemin+="droite.png";
            else {chemin+="droite.png";compteur=0;}
            setImage(chemin);
            int deplacementJoueur=position.x+(maFenetre->resolutionTaille.x/100);
            for (int i=0;i<maFenetre->listElementMap.length();i++)
            {

                if(
                        maFenetre->listElementMap[i]->franchissable==false &&

                        (
                        (position.y+taille.y/1.5>=maFenetre->listElementMap[i]->position.y &&
                        position.y-1-taille.y/2.5+taille.y<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY) ||
                        (
                        position.y+taille.y>=maFenetre->listElementMap[i]->position.y &&
                        position.y+taille.y<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY/2)
                        /*||
                        (
                        position.y-maFenetre->tailleBlockY>=maFenetre->listElementMap[i]->position.y &&
                        position.y-maFenetre->tailleBlockY+maFenetre->tailleBlockY<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY)
                        */)&&
                        position.x+taille.x>=maFenetre->listElementMap[i]->position.x &&// position.y+tailleBlockY car on vérifie par rapport à l'enplacement des pieds
                        position.x<maFenetre->listElementMap[i]->position.x
                        )
                {
                    if (deplacementJoueur+taille.x>=maFenetre->listElementMap[i]->position.x)
                    {
                        if(pousser==true)
                        {
                            Bombe *bombe=dynamic_cast<Bombe *>(maFenetre->listElementMap[i]);
                            if ( bombe!=0)
                            {
                                qDebug() << "Bombe devant";
                                bombe->deplacementDroite=true;
                                bombe->deplacementBas=false;
                                bombe->deplacementHaut=false;
                                bombe->deplacementGauche=false;
                            }
                        }
                        deplacementJoueur=position.x+((maFenetre->listElementMap[i]->position.x)-(position.x+taille.x))+taille.x/10;
                        Coord positionJoueur ;
                        positionJoueur.x=int(position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX;
                        positionJoueur.y=int((position.y)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY+maFenetre->tailleBlockY;
                        for (int i  = 0; i<maFenetre->listElementMap.length();i++)
                        {
                            if(maFenetre->listElementMap[i]->position.x==positionJoueur.x && maFenetre->listElementMap[i]->position.y > position.y+maFenetre->tailleBlockY/2.5 && maFenetre->listElementMap[i]->position.y <= position.y+maFenetre->tailleBlockY)
                            {
                                bool caseOK=true;
                                for (int z  = 0; z<maFenetre->listElementMap.length();z++)
                                {
                                    if(maFenetre->listElementMap[z]->position.x==positionJoueur.x+maFenetre->tailleBlockX && maFenetre->listElementMap[z]->position.y >= position.y - maFenetre->tailleBlockY/1.2 && maFenetre->listElementMap[z]->position.y <= position.y)
                                    {
                                        if (maFenetre->listElementMap[z]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }

                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.y-=(maFenetre->resolutionTaille.y/100);
                                }
                            }
                            else if(maFenetre->listElementMap[i]->position.x==positionJoueur.x && maFenetre->listElementMap[i]->position.y < position.y+maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.y > position.y)
                            {
                                bool caseOK=true;
                                for (int z  = 0; z<maFenetre->listElementMap.length();z++)
                                {
                                    if(maFenetre->listElementMap[z]->position.x==positionJoueur.x+maFenetre->tailleBlockX && maFenetre->listElementMap[z]->position.y < position.y+maFenetre->tailleBlockY && maFenetre->listElementMap[z]->position.y > position.y)
                                    {
                                        if (maFenetre->listElementMap[z]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.y=position.y+(maFenetre->resolutionTaille.y/100);
                                }
                            }

                        }
                    }
                }
            }
            setPosition(deplacementJoueur,position.y);
            miseAJour();
        }
        else if (deplacementGauche==true)
        {
            compteur++;
            if (compteur>=1 && compteur<6)chemin+="marcheGauche1.png";
            else if (compteur>=6 && compteur<10)chemin+="gauche.png";
            else if (compteur>=10 && compteur<16){chemin+="marcheGauche2.png";}
            else if (compteur>=16 && compteur<19)chemin+="gauche.png";
            else {chemin+="gauche.png";compteur=0;}
            setImage(chemin);
            int deplacementJoueur=position.x-(maFenetre->resolutionTaille.x/100);
            for (int i=0;i<maFenetre->listElementMap.length();i++)
            {

                if(
                        maFenetre->listElementMap[i]->franchissable==false &&

                        (
                        (position.y+taille.y/2>=maFenetre->listElementMap[i]->position.y &&
                        position.y-2-taille.y/2.5+taille.y<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY) ||
                        (
                        position.y+taille.y>=maFenetre->listElementMap[i]->position.y &&
                        position.y+taille.y<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY/2)
                        /*||
                        (
                        position.y-maFenetre->tailleBlockY>=maFenetre->listElementMap[i]->position.y &&
                        position.y-maFenetre->tailleBlockY+maFenetre->tailleBlockY<=maFenetre->listElementMap[i]->position.y+maFenetre->tailleBlockY)
                        */)&&
                        position.x>maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX &&// position.y+tailleBlockY car on vérifie par rapport à l'enplacement des pieds
                        position.x<maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX*2
                        )
                {
                    if (deplacementJoueur<=maFenetre->listElementMap[i]->position.x+maFenetre->tailleBlockX)
                    {
                        if(pousser==true)
                        {
                            Bombe *bombe=dynamic_cast<Bombe *>(maFenetre->listElementMap[i]);
                            if ( bombe!=0)
                            {
                                qDebug() << "Bombe devant";
                                bombe->deplacementGauche=true;
                                bombe->deplacementDroite=false;
                                bombe->deplacementHaut=false;
                                bombe->deplacementBas=false;
                            }
                        }
                        deplacementJoueur=position.x;
                        Coord positionJoueur ;
                        positionJoueur.x=int(position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX;
                        positionJoueur.y=int((position.y)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY+maFenetre->tailleBlockY;
                        for (int i  = 0; i<maFenetre->listElementMap.length();i++)
                        {
                            if(maFenetre->listElementMap[i]->position.x==positionJoueur.x && maFenetre->listElementMap[i]->position.y > position.y+maFenetre->tailleBlockY/2.5 && maFenetre->listElementMap[i]->position.y <= position.y+maFenetre->tailleBlockY)
                            {
                                bool caseOK=true;
                                for (int z  = 0; z<maFenetre->listElementMap.length();z++)
                                {
                                    if(maFenetre->listElementMap[z]->position.x==positionJoueur.x && maFenetre->listElementMap[z]->position.y >= position.y - maFenetre->tailleBlockY/1.2 && maFenetre->listElementMap[z]->position.y <= position.y)
                                    {
                                        if (maFenetre->listElementMap[z]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }

                                for(int y = 0;y<maFenetre->listElementMap.length();y++)// on verifie la cas ou il y a un mur au dessus du sol
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    position.y-=(maFenetre->resolutionTaille.y/100);
                                }
                            }
                            else if(maFenetre->listElementMap[i]->position.x==positionJoueur.x && maFenetre->listElementMap[i]->position.y < position.y+maFenetre->tailleBlockY && maFenetre->listElementMap[i]->position.y > position.y)
                            {
                                bool caseOK=true;
                                for (int z  = 0; z<maFenetre->listElementMap.length();z++)
                                {
                                    if(maFenetre->listElementMap[z]->position.x==positionJoueur.x && maFenetre->listElementMap[z]->position.y < position.y+maFenetre->tailleBlockY && maFenetre->listElementMap[z]->position.y > position.y)
                                    {
                                        if (maFenetre->listElementMap[z]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                for(int y = 0;y<maFenetre->listElementMap.length();y++)
                                {
                                    if (maFenetre->listElementMap[y]->position.x==maFenetre->listElementMap[i]->position.x &&
                                            maFenetre->listElementMap[y]->position.y==maFenetre->listElementMap[i]->position.y)
                                    {
                                        if (maFenetre->listElementMap[y]->franchissable==false)
                                        {
                                            caseOK=false;
                                        }
                                    }
                                }
                                if (maFenetre->listElementMap[i]->franchissable==true && caseOK==true)
                                {
                                    //position.y=position.y-(maFenetre->resolutionTaille.y/100);
                                }
                            }

                        }
                    }
                }
            }
            setPosition(deplacementJoueur,position.y);
            miseAJour();
        }

    }

}

void Personnage::poserBombe(Personnage* joueur1)
{

    qDebug() << "Test";
    if (joueur1->numeroObjet==numeroObjet)
    {
        bool poserBombeOK=true;
        if (!maFenetre->listBombe.isEmpty())
        {
            for (int i=0;i<maFenetre->listBombe.length();i++)
            {
                if ((joueur1->position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX==maFenetre->listBombe[i]->position.x
                        &&
                        ((joueur1->position.y+maFenetre->tailleBlockY/2)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY==maFenetre->listBombe[i]->position.y)
                {
                    poserBombeOK=false;
                }
            }
        }
        if(poserBombeOK==true && bombe>0)
        {
            bombe--;
            Bombe *bombe=new Bombe(maFenetre,typeBombe,joueur1->flamme);
            bombe->setTaille(maFenetre->tailleBlockX,maFenetre->tailleBlockY);
            if (joueur1->typeBombe==1)bombe->setImage("../bombe/bombe1.png");
            if (joueur1->typeBombe==3)bombe->setImage("../bombe/bombe1.png");
            if (joueur1->typeBombe==4)bombe->setImage("../bombe/bombe1.png");
            if (joueur1->typeBombe==2)bombe->setImage("../bombe/bombeType2_1.png");
            bombe->joueur=joueur;
            Coord positionBombe;
            positionBombe.x=(joueur1->position.x/maFenetre->tailleBlockX)*maFenetre->tailleBlockX;
            positionBombe.y=((joueur1->position.y+maFenetre->tailleBlockY/2)/maFenetre->tailleBlockY)*maFenetre->tailleBlockY;
            bombe->setPosition(positionBombe.x,positionBombe.y);
            bombe->miseAJour();
            maFenetre->listBombe.push_back(bombe);
            maFenetre->listElementMap.push_back(bombe);
            maFenetre->listElementMapInfranchissable.push_back(bombe);
            maFenetre->listElementMapSensibleFlamme.push_back(bombe);

            connect(bombe,SIGNAL(bombeExplose(Coord,short int,short int,short int)),maFenetre,SLOT(explosionBombe(Coord,short int,short int,short int)));
            maFenetre->compteurNombreExplosionBombe++;
        }

    }

}
