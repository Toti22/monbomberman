#ifndef BONUS_H
#define BONUS_H
#include "elementmap.h"
#include "coord.h"
class Bonus : public ElementMapSensibleFlamme
{
    friend class Personnage;
    friend class MonMenu;
    friend class ElementMap;
    friend class Bombe;
    Q_OBJECT
public:
    Bonus(MonMenu *fenetre,int,Coord);
    static int typeMax;
protected:
    QTimer* timer;
    int type;
    bool bonusAttrape;
    int compteurMortBonus;
private slots:
    virtual void destructionParFlamme(int);
    void attrapeParJoueur();
};

#endif // BONUS_H
