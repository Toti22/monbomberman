#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "coord.h"
#include <QWidget>
#include <QAudioDecoder>
#include <QList>
#include <QMainWindow>
#include <QtNetwork>
class QLabel;

class QPushButton;
class QSpinBox;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class Personnage;
class Mur;
class Sol;
class MurIncassable;
class QTimer;
class ElementMap;
class Bombe;
class ElementMapSensibleFlamme;
class Flamme;
class QLineEdit;
class QSound;
namespace Ui {
class QWidget;
}

class MonMenu : public QWidget
{
    Q_OBJECT
    friend class Personnage;
    friend class ElementMap;
    friend class Bombe;
    friend class Bonus;
    friend class Mur;
public:
    static Coord tailleFenetre;
    bool event(QEvent *eventTemp);
    MonMenu();
    ~MonMenu();


    Coord getResolutionTaille(){return resolutionTaille;}
    QList<ElementMap*>getListElementMap(){return listElementMap;}
    int getTailleBlockX(){return tailleBlockX;}
    int getTailleBlockY(){return tailleBlockY;}

signals:
    void personnageDeplacement();
    void elementToucheFlamme(int);
    void poserBombeSignal(Personnage*);
    void signalToucheChoisi(int,int,int);
    void signalExplosionBombe();

private slots:
    void selectionResolution();
    void changementResolution();
    void retirerChoixResolution();
    void retirerChoixSolo();
    void choixSolo();
    void jeuStandard();
    void choixOption();
    void choixConfigurationTouche();
    void retirerChoixOption();
    void retirerChoixConfigurationTouche();
    void explosionBombe(Coord,short int,short int,short int);
    void clickBoutonConfigurationTouche(int touche, int joueur, int numeroBoutonConfiguration);
    void ajoutJoueur();
    void choixMultijoueur();
    void retirerChoixMultijoueur();
    void choixJeuxLocal();
    void retirerChoixJeuxLocal();
    void creerPartieLocal();
    void creerPartieLocal2();
    void rejoindrePartieLocal();

    void connexion();
    void connexion_OK();
    void donneesRecuesClient();
    void donneesRecuesServeur();
    void nouvelleConnexion();
    void deconnexionClient();
    void lancerPartieLocal();

private:

    QTcpServer *serveur;
    QTcpSocket *socketClient;
    QList<QTcpSocket *> listClients;
    quint16 tailleMessage;
    QString messageRecu;
    QString pseudo;
    bool clientAtenteReponseDeplacement;
    bool enAttenteDeJoueur;
    bool partieLance;
    bool partieLocal;
    bool host;
    bool client;
    bool paquetDeplacementBasEnvoye;
    bool paquetDeplacementHautEnvoye;
    bool paquetDeplacementGaucheEnvoye;
    bool paquetDeplacementDroiteEnvoye;

    int nbMessageATraite;

    QEvent *evenement;
    QTimer *timerJoueur1;
    QKeyEvent *ke;//connaitr ela touche appuye

    bool objetPortail;//permet au joueur de ce tp et les flammes
    bool objetLanceBombe;
    bool morceauMessageRecu;
    bool objetBouclier;
    bool objetPousserBombe;
    bool objetMalus;
    bool bombeTelecommander;
    bool bombeTransperceMur;
    bool deplacementJoueurConnecte;
    bool appuiToucheConfiguration;
    bool enAttenteIdentifiant; // lorsque le client attend son identifiant, il prendra le prochain paquet comme reference pour son identifiant

    int nombreClient;
    int nombreJoueur;
    int numeroClient;
    int nombreBlockX;
    int nombreBlockY;
    int tailleBlockX;
    int tailleBlockY;
    int compteur;// il va nous etre utile pour l'animation du personnage
    int compteurNombreExplosionBombe;// va nous etre utile pour savoir qu'elle flamme sont à supprimer
    int compteurExplosionBombe;//problème lorsque deux signaux sont envoyé en simultané. On va compter le nombre de boucle et quitter la boucle si nécessaire
    int joueurConfigurationTouche;
    int numeroBoutonConfiguration;

    QList<Bombe*>listBombe;
    QList<Mur*>listMur;
    QList<Sol*>listSol;
    QList<MurIncassable*>listMurIncassable;
    QList<ElementMap*>listElementMap;
    QList<Flamme*>listFlamme;
    QList<ElementMapSensibleFlamme*>listElementMapSensibleFlamme;
    QList<ElementMap*>listElementMapInfranchissable;
    QList<Personnage*>listPersonnage;
    QList<QWidget*>listWidgetTouche;
    QList<QWidget*>listWidget;
    QList<QGridLayout*>listGridLayoutTouche;
    QList<QPushButton*>listBouton;
    QList<QLineEdit*>listLineEdit;
    QList<QSpinBox*>listSpinBox;
    QList<QLabel*>listLabel;
    QList<QString*>listPseudo;

    Personnage *personnage;
    Personnage* joueur1;
    Personnage* joueur2;

    QLabel *imageMenu;
    QLabel *test;
    QLabel *labelMenuConfigurationToucheNombreJoueur;

    QSpinBox *spinBoxNombreJoueur;

    QPushButton *resolutionBouton;QPushButton *boutonResolution1;QPushButton *boutonResolution2;QPushButton *boutonRetour;
    QPushButton *boutonSolo;
    QPushButton *boutonMultiJoueur;QPushButton *boutonOption;
    QPushButton *boutonJeuStandard;QPushButton *boutonJeuPersonnalise; QPushButton *boutonConfigurationTouche;
    QPushButton *boutonOK;
    QPushButton *multiLocal;QPushButton *multiEnLigne;


    QGridLayout *gridLayoutMenu;
    QGridLayout *gridLayoutMenuConfigurationTouche;
    QGridLayout *gridLayoutControle;
    QHBoxLayout *LayoutMenuConfigurationToucheNombreJoueur;

    QVBoxLayout *layoutResolution;
    QVBoxLayout *layoutSolo;
    QVBoxLayout *layoutMultijoueur;
    QVBoxLayout *layoutOption;

    QSound *musiqueMenu;

    Coord resolutionTaille;
    Coord positionFenetre;
    bool boutonResolutionActive;
    bool typePartie;//FALSE = Partie standard || TRUE = partie personnalise

    void destructeur();
    void constructeur();
    void creationTerrain();
    void envoieMessageServeur(QString message);
    void envoieMessageClient(QString message,QTcpSocket *socket);
    void envoyerMessageATous(QString message);
    void ajouterIntAString(QString *, int, int);
    void clearListMenu();

};

#endif // MAINWINDOW_H
