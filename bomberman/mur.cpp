#include "mur.h"
#include "mainwindow.h"
#include <QPushButton>
#include <math.h>
#include <QObject>
#include <QLayout>
#include "elementmap.h"
#include"mainwindow.h"
#include "bonus.h"
#include <ctime>

Mur::Mur(MonMenu* fenetre) : ElementMapSensibleFlamme(fenetre),dejaConnecte(false)
{
    QPixmap pixMapMur("../decor/mur.png");
    pixMapMur.scaled(taille.x,taille.y,Qt::IgnoreAspectRatio);
    image->setPixmap(pixMapMur);
    franchissable=false;
    type=2;
}
Mur::~Mur()
{
    delete image;
}

void Mur::destruction()
{

}

void Mur::destructionParFlamme(int numeroElement)
{
    if(numeroElement==numeroObjet)
    {
        if(dejaConnecte==false)
        {
            dejaConnecte=true;
            timer=new QTimer();
            timer->setInterval(100);
            timer->start();
            connect(timer,SIGNAL(timeout()),this,SLOT(destrutctionRetardement()));
        }
    }
}

void Mur::destrutctionRetardement()
{
    if (maFenetre->partieLocal==true)
    {
        if(maFenetre->host==true)
        {
            int nombreAleat = (rand() % (Bonus::typeMax - 0))+1 ;
            QString message("");
            message+="05";
            maFenetre->ajouterIntAString(&message,nombreAleat,3);
            maFenetre->ajouterIntAString(&message,position.x,3);
            maFenetre->ajouterIntAString(&message,position.y,3);
            maFenetre->envoyerMessageATous(message);
            Bonus *bonus = new Bonus(maFenetre,nombreAleat,position);
            maFenetre->listElementMapSensibleFlamme.push_back(bonus);
            this->setImage("../");
            this->setPosition(0,0);
            this->miseAJour();
            bool bienDeco=disconnect(timer,SIGNAL(timeout()),this,SLOT(destrutctionRetardement()));
            delete timer;
            timer=NULL;
        }
        else if (maFenetre->client==true)
        {
            this->setImage("../");
            this->setPosition(0,0);
            this->miseAJour();
            bool bienDeco=disconnect(timer,SIGNAL(timeout()),this,SLOT(destrutctionRetardement()));
            delete timer;
            timer=NULL;
        }
    }
    else
    {
        int nombreAleat = (rand() % (Bonus::typeMax - 0))+1 ;
        Bonus *bonus = new Bonus(maFenetre,nombreAleat,position);
        maFenetre->listElementMapSensibleFlamme.push_back(bonus);
        this->setImage("../");
        this->setPosition(0,0);
        this->miseAJour();
        //this->deleteLater();
        bool bienDeco=disconnect(timer,SIGNAL(timeout()),this,SLOT(destrutctionRetardement()));
        delete timer;
        timer=NULL;
    }


}

