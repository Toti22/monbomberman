﻿#include "mainwindow.h"
#include "coord.h"
#include <QPushButton>
#include <QObject>
#include <QLayout>
#include "personnage.h"
#include <QSound>
#include <QtNetwork>

#include <QMainWindow>
#include <ctime>
#include <boutontouche.h>
#include <QLineEdit>
#include <QSpinBox>
#include <QList>
#include <QDebug>
#include "main.h"
#include "mur.h"
#include "sol.h"
#include "personnage.h"
#include "murincassable.h"
#include "elementmap.h"
#include <QEvent>
#include <QKeyEvent>
#include <QTimer>
#include <QSpinBox>
#include <QHBoxLayout>
#include "boutontouche.h"
#include <math.h>

#define NB_MAX_JOUEUR 4

MonMenu::MonMenu() : QWidget(),boutonResolutionActive(false),boutonResolution1(NULL),
    boutonResolution2(NULL),gridLayoutMenu(NULL),layoutSolo(NULL),layoutMultijoueur(NULL),layoutOption(NULL),layoutResolution(NULL),
    boutonRetour(NULL),boutonJeuPersonnalise(NULL),boutonJeuStandard(NULL),
    compteur(0),evenement(NULL),compteurNombreExplosionBombe(0),compteurExplosionBombe(0),deplacementJoueurConnecte(false),boutonOK(NULL),
    appuiToucheConfiguration(false),multiEnLigne(NULL),multiLocal(NULL),host(NULL),client(NULL),nombreClient(0),messageRecu(""),
    morceauMessageRecu(false),partieLance(false),partieLocal(false),nbMessageATraite(0),boutonConfigurationTouche(NULL)


{
    listPseudo.clear();
    messageRecu.clear();
    musiqueMenu = new QSound("../audio/menu.wav");
    musiqueMenu->setLoops(QSound::Infinite);
    musiqueMenu->play();
    resolutionTaille.x=800;
    resolutionTaille.y=600;
    positionFenetre.x=((1366-resolutionTaille.x)/2);
    positionFenetre.y=((738-resolutionTaille.y)/2);
    setWindowState(windowState() ^ Qt::WindowNoState);
    setGeometry(positionFenetre.x,positionFenetre.y,resolutionTaille.x,resolutionTaille.y);
    imageMenu = new QLabel(this);
    QPixmap pixmapImageMenu("../menu/menu.png");
    pixmapImageMenu =  pixmapImageMenu.scaled(resolutionTaille.x,resolutionTaille.y,Qt::IgnoreAspectRatio);
    imageMenu->setPixmap(pixmapImageMenu);
    imageMenu->show();
    constructeur();
}

void MonMenu::constructeur()
{
    delete layout();
    resolutionBouton = new QPushButton("RESOLUTION");
    resolutionBouton->setFont(QFont("Comic Sans MS", 14));

    boutonSolo = new QPushButton("SOLO");
    boutonSolo->setFont(QFont("Comic Sans MS",14));

    boutonMultiJoueur = new QPushButton("MultiJoueur");
    boutonMultiJoueur->setFont(QFont("Comic Sans MS",14));

    boutonOption = new QPushButton("Option");
    boutonOption->setFont(QFont("Comic Sans MS",14));

    gridLayoutMenu = new QGridLayout;

    layoutResolution = new QVBoxLayout;
    layoutResolution->addWidget(resolutionBouton);
    layoutSolo = new QVBoxLayout;
    layoutSolo->addWidget(boutonSolo);
    layoutMultijoueur = new QVBoxLayout;
    layoutMultijoueur->addWidget(boutonMultiJoueur);
    layoutOption = new QVBoxLayout;
    layoutOption->addWidget(boutonOption);

    gridLayoutMenu->addLayout(layoutSolo,1,0);
    gridLayoutMenu->addLayout(layoutMultijoueur,1,1);
    gridLayoutMenu->addLayout(layoutOption,1,2);
    gridLayoutMenu->addLayout(layoutResolution,1,3);

    QObject::connect(resolutionBouton,SIGNAL(clicked()),this, SLOT(selectionResolution()));
    QObject::connect(boutonSolo,SIGNAL(clicked()),this, SLOT(choixSolo()));
    connect(boutonOption,SIGNAL(clicked()),this,SLOT(choixOption()));
    connect(boutonMultiJoueur,SIGNAL(clicked()),this, SLOT(choixMultijoueur()));

    this->setLayout(gridLayoutMenu);


}

void MonMenu::destructeur()
{
    for (int i =0;i<listWidget.length();i++)
    {
        if (listWidget[i]!=NULL)
        {
            delete listWidget[i];
            listWidget[i]=NULL;
        }
    }
    QLayoutItem *child;
    for (int i = 0 ; i<listGridLayoutTouche.length();i++)
    {
        while ((child = listGridLayoutTouche[i]->takeAt(0)) != 0)
        {
            listGridLayoutTouche[i]->removeItem(child);
            if (child!=NULL)delete child;
            child=NULL;
        }
    }
    while ((child = gridLayoutMenu->takeAt(0)) != 0)
    {
        gridLayoutMenu->removeItem(child);
        if (child!=NULL)delete child;
        child=NULL;
    }


    delete gridLayoutMenu;
    gridLayoutMenu=NULL;

    if(resolutionBouton!=NULL)
    {
        delete resolutionBouton;
        resolutionBouton=NULL;
    }

    if(boutonResolution1!=NULL)
    {
        delete boutonResolution1;
        boutonResolution1=NULL;
    }

    if(boutonResolution2!=NULL)
    {
        delete boutonResolution2;
        boutonResolution2=NULL;
    }

    if(boutonSolo!=NULL)
    {
        delete boutonSolo;
        boutonSolo=NULL;
    }

    if(boutonMultiJoueur!=NULL)
    {
        delete boutonMultiJoueur;
        boutonMultiJoueur=NULL;
    }

    if (boutonOption!=NULL)
    {
        delete boutonOption;
        boutonOption=NULL;
    }

    if(boutonJeuStandard!=NULL)
    {
        delete boutonJeuStandard;
        boutonJeuStandard=NULL;
    }

    if(boutonJeuPersonnalise!=NULL)
    {
        delete boutonJeuPersonnalise;
        boutonJeuPersonnalise=NULL;
    }

    if(imageMenu != NULL)
    {
        delete imageMenu;
        imageMenu=NULL;
    }

    if(boutonRetour!=NULL)
    {
        delete boutonRetour;
        boutonRetour=NULL;
    }

}

MonMenu::~MonMenu()
{
    destructeur();
}

void MonMenu::retirerChoixSolo()
{

    if (boutonJeuStandard!=NULL && boutonJeuPersonnalise!=NULL && boutonRetour!=NULL)
    {
        connect(boutonSolo,SIGNAL(clicked()),this, SLOT(choixSolo()));
        layoutSolo->removeWidget(boutonJeuStandard);
        layoutSolo->removeWidget(boutonJeuPersonnalise);
        layoutSolo->removeWidget(boutonRetour);
        delete boutonJeuStandard;
        delete boutonJeuPersonnalise;
        delete boutonRetour;
        boutonJeuStandard=NULL;
        boutonJeuPersonnalise=NULL;
        boutonRetour=NULL;
    }
}

void MonMenu::retirerChoixResolution()
{
    if (boutonResolution1!=NULL && boutonResolution2!=NULL && boutonRetour!=NULL)
    {
    layoutResolution->removeWidget(boutonResolution1);
    layoutResolution->removeWidget(boutonResolution2);
    layoutResolution->removeWidget(boutonRetour);
    delete boutonResolution1;
    delete boutonResolution2;
    delete boutonRetour;
    boutonResolution1=NULL;
    boutonResolution2=NULL;
    boutonRetour=NULL;
    QObject::connect(resolutionBouton,SIGNAL(clicked()),this, SLOT(selectionResolution()));
    disconnect(boutonRetour,SIGNAL(clicked()),this,0);
    }
}

void MonMenu::selectionResolution()
{
    retirerChoixSolo();
    retirerChoixOption();
    retirerChoixMultijoueur();
    disconnect(resolutionBouton,SIGNAL(clicked()),this,SLOT(selectionResolution()));
    boutonResolution1 = new QPushButton("800x600",this);
    boutonResolution2 = new QPushButton("1366x768",this);
    boutonRetour = new QPushButton ("Retour",this);
    boutonResolution1->move(resolutionTaille.x/9.5,resolutionTaille.y/1.25);
    boutonResolution2->move(resolutionTaille.x/9.5,resolutionTaille.y/1.15);
    boutonRetour->move(resolutionTaille.x/9.5,resolutionTaille.y/1.05);
    boutonResolution1->show();
    boutonResolution2->show();
    boutonRetour->show();


    QObject::connect(boutonResolution1,SIGNAL(clicked()),this, SLOT(changementResolution()));
    QObject::connect(boutonResolution2,SIGNAL(clicked()),this, SLOT(changementResolution()));
    QObject::connect(boutonRetour,SIGNAL(clicked()),this, SLOT(retirerChoixResolution()));

    layoutResolution->addWidget(boutonResolution1);
    layoutResolution->addWidget(boutonResolution2);
    layoutResolution->addWidget(boutonRetour);
    this->setLayout(layoutResolution);

}

void MonMenu::changementResolution()
{

    if (boutonResolution1->underMouse()){resolutionTaille.x=800;resolutionTaille.y=600;}
    if (boutonResolution2->underMouse()){resolutionTaille.x=1366;resolutionTaille.y=738;}
    positionFenetre.x=((1366-resolutionTaille.x)/2);
    positionFenetre.y=((738-resolutionTaille.y)/2);
    if (resolutionTaille.x==1366 && resolutionTaille.y==738){if (!this->isFullScreen())setWindowState(windowState() ^ Qt::WindowFullScreen);}
    else setGeometry(positionFenetre.x,positionFenetre.y,resolutionTaille.x,resolutionTaille.y);

    QPixmap pixmapImageMenu("../menu/menu.png");
    pixmapImageMenu =  pixmapImageMenu.scaled(resolutionTaille.x,resolutionTaille.y,Qt::IgnoreAspectRatio);
    imageMenu->setGeometry(0,0,resolutionTaille.x,resolutionTaille.y);
    imageMenu->setPixmap(pixmapImageMenu);
    resolutionBouton->setFont(QFont("Comic Sans MS", 14));
    resolutionBouton->move(
                           (resolutionTaille.x/10)-(resolutionBouton->size().height()),
                           resolutionTaille.y/1.1-resolutionBouton->size().width());
    resolutionBouton->show();

    boutonResolutionActive=true;

}

void MonMenu::choixSolo()
{
    disconnect(boutonSolo,SIGNAL(clicked()),this, SLOT(choixSolo()));
    retirerChoixResolution();
    retirerChoixOption();
    retirerChoixMultijoueur();
    boutonJeuStandard = new QPushButton("Partie Standard",this);
    boutonJeuStandard->setFont(QFont("Comic Sans MS",10));
    boutonJeuPersonnalise = new QPushButton("Partie Personnalise",this);
    boutonJeuPersonnalise->setFont(QFont("Comic Sans MS",10));
    boutonRetour = new QPushButton("Retour",this);
    boutonRetour->setFont(QFont("Comic Sans MS",10));

    layoutSolo->addWidget(boutonJeuPersonnalise);
    layoutSolo->addWidget(boutonJeuStandard);
    layoutSolo->addWidget(boutonRetour);
    QObject::connect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixSolo()));
    QObject::connect(boutonJeuStandard,SIGNAL(clicked()),this,SLOT(jeuStandard()));
}

void MonMenu::choixOption()
{
    retirerChoixResolution();
    retirerChoixSolo();
    retirerChoixMultijoueur();
    disconnect(boutonOption,SIGNAL(clicked()),this,SLOT(choixOption()));
    boutonConfigurationTouche = new QPushButton("Configurer touche",this);
    boutonRetour = new QPushButton("Retour",this);
    boutonConfigurationTouche->setFont(QFont("Comic Sans MS",10));
    boutonRetour->setFont(QFont("Comic Sans MS",10));

    boutonConfigurationTouche->show();
    boutonRetour->show();

    layoutOption->addWidget(boutonConfigurationTouche);
    layoutOption->addWidget(boutonRetour);

    connect(boutonConfigurationTouche,SIGNAL(clicked()),this,SLOT(choixConfigurationTouche()));
    connect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixOption()));
    this->setLayout(layoutOption);

}

void MonMenu::choixConfigurationTouche()
{
    disconnect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixOption()));
    destructeur();

    imageMenu = new QLabel(this);
    QPixmap pixmapImageMenu("../menu/menu.png");
    pixmapImageMenu =  pixmapImageMenu.scaled(resolutionTaille.x,resolutionTaille.y,Qt::IgnoreAspectRatio);
    imageMenu->setPixmap(pixmapImageMenu);
    imageMenu->show();


    spinBoxNombreJoueur = new QSpinBox();

    boutonOK = new QPushButton("OK");
    boutonOK->setFont(QFont("Comic Sans MS", 14));

    boutonRetour = new QPushButton("Retour");
    boutonRetour->setFont(QFont("Comic Sans MS",14));

    connect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixConfigurationTouche()));



    labelMenuConfigurationToucheNombreJoueur = new QLabel();
    spinBoxNombreJoueur->setMaximum(8);
    spinBoxNombreJoueur->setMinimum(2);
    spinBoxNombreJoueur->setValue(2);

    labelMenuConfigurationToucheNombreJoueur->setText("Nombre de joueur");
    labelMenuConfigurationToucheNombreJoueur->setFont(QFont("Comic Sans MS", 14));

    listWidgetTouche.push_back(labelMenuConfigurationToucheNombreJoueur);
    listWidgetTouche.push_back(boutonOK);
    listWidgetTouche.push_back(boutonRetour);
    listWidgetTouche.push_back(spinBoxNombreJoueur);

    LayoutMenuConfigurationToucheNombreJoueur = new QHBoxLayout;
    LayoutMenuConfigurationToucheNombreJoueur->addWidget(labelMenuConfigurationToucheNombreJoueur);
    LayoutMenuConfigurationToucheNombreJoueur->addWidget(spinBoxNombreJoueur);
    LayoutMenuConfigurationToucheNombreJoueur->addWidget(boutonOK);
    LayoutMenuConfigurationToucheNombreJoueur->addWidget(boutonRetour);
    gridLayoutControle = new QGridLayout;
    gridLayoutControle->addLayout(LayoutMenuConfigurationToucheNombreJoueur,0,0);

    LayoutMenuConfigurationToucheNombreJoueur->setAlignment(spinBoxNombreJoueur,Qt::AlignTop);
    LayoutMenuConfigurationToucheNombreJoueur->setAlignment(labelMenuConfigurationToucheNombreJoueur,Qt::AlignTop);
    LayoutMenuConfigurationToucheNombreJoueur->setAlignment(boutonOK,Qt::AlignTop);
    LayoutMenuConfigurationToucheNombreJoueur->setAlignment(boutonRetour,Qt::AlignTop);

    connect(boutonOK,SIGNAL(clicked()),this,SLOT(ajoutJoueur()));



    ajoutJoueur();

}

void MonMenu::choixMultijoueur()
{
    disconnect(boutonMultiJoueur,SIGNAL(clicked()),this, SLOT(choixMultijoueur()));
    retirerChoixResolution();
    retirerChoixOption();
    retirerChoixSolo();
    multiLocal = new QPushButton("Partie local",this);
    multiLocal->setFont(QFont("Comic Sans MS",10));
    multiEnLigne = new QPushButton("Partie en ligne",this);
    multiEnLigne->setFont(QFont("Comic Sans MS",10));
    boutonRetour= new QPushButton("retour",this);
    boutonRetour->setFont(QFont("Comic Sans MS",10));
    layoutMultijoueur->addWidget(multiLocal);
    layoutMultijoueur->addWidget(multiEnLigne);
    layoutMultijoueur->addWidget(boutonRetour);
    connect(multiLocal,SIGNAL(clicked()),this, SLOT(choixJeuxLocal()));
    connect(boutonRetour,SIGNAL(clicked()),this, SLOT(retirerChoixMultijoueur()));
}

void MonMenu::choixJeuxLocal()
{
    disconnect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixMultijoueur()));
    destructeur();

    imageMenu = new QLabel(this);
    QPixmap pixmapImageMenu("../menu/menu.png");
    pixmapImageMenu =  pixmapImageMenu.scaled(resolutionTaille.x,resolutionTaille.y,Qt::IgnoreAspectRatio);
    imageMenu->setPixmap(pixmapImageMenu);
    imageMenu->show();

    QGridLayout *gridLayout = new QGridLayout;
    listGridLayoutTouche.push_back(gridLayout);
    QPushButton * bouton = new QPushButton();
    bouton->setText("Créer une partie");
    listBouton.push_back(bouton);
    bouton = new QPushButton();
    bouton->setText("joindre une partie");
    listBouton.push_back(bouton);
    boutonRetour = new QPushButton();
    boutonRetour->setText("retour");
    listBouton.push_back(boutonRetour);
    for(int i = 0 ; i<listBouton.length();i++)
    {
        gridLayout->addWidget(listBouton[i]);
    }
    setLayout(gridLayout);
    connect(boutonRetour,SIGNAL(clicked()),this,SLOT(retirerChoixJeuxLocal()));
    connect(listBouton[0],SIGNAL(clicked()),this,SLOT(creerPartieLocal()));
    connect(listBouton[1],SIGNAL(clicked()),this,SLOT(rejoindrePartieLocal()));
}

void MonMenu::creerPartieLocal()
{
    for(int i = 0 ; i<listWidget.length();i++)
    {
        delete listWidget[i];
    }
    listWidget.clear();
    for (int i = 0; i< listBouton.length();i++)
    {
        delete listBouton[i];
    }
    listBouton.clear();
    for (int i = 0; i< listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
    }
    listGridLayoutTouche.clear();
    QGridLayout *gridLayout = new QGridLayout();
    listGridLayoutTouche.push_back(gridLayout);
    QLabel *labelPseudo = new QLabel();
    listLabel.push_back(labelPseudo);
    labelPseudo->setText("votre speudo :");
    QLineEdit * lineEditPseudo = new QLineEdit();
    listLineEdit.push_back(lineEditPseudo);
    lineEditPseudo->setMaxLength(20);
    QPushButton * boutonCreerServeur = new QPushButton();
    boutonCreerServeur->setText("creer serveur");
    listWidget.push_back(labelPseudo);
    listWidget.push_back(lineEditPseudo);
    listWidget.push_back(boutonCreerServeur);
    connect(boutonCreerServeur,SIGNAL(clicked()),this,SLOT(creerPartieLocal2()));
    for (int i=0 ; i<listWidget.length();i++)
    {
        gridLayout->addWidget(listWidget[i],0,i);
    }
    setLayout(gridLayout);

}

void MonMenu::creerPartieLocal2()
{
    if (listLineEdit[0]->text()!="")pseudo = listLineEdit[0]->text();
    else pseudo = "pasDePseudo";
    for(int i = 0 ; i<listWidget.length();i++)
    {
        delete listWidget[i];
    }
    listWidget.clear();
    for (int i = 0; i< listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
    }
    listGridLayoutTouche.clear();
    //delete layout();
    enAttenteDeJoueur=true;

    QGridLayout *gridLayout = new QGridLayout;
    QGridLayout *gridLayout2 = new QGridLayout;
    listGridLayoutTouche.push_back(gridLayout);
    listGridLayoutTouche.push_back(gridLayout2);
    QLabel *etatServeur = new QLabel();

    etatServeur->setMaximumWidth(resolutionTaille.x);
    QPixmap pixmapImage("../gris.png");
    pixmapImage =  pixmapImage.scaled(etatServeur->width(),etatServeur->height(),Qt::IgnoreAspectRatio);
    etatServeur->setPixmap(pixmapImage);
    etatServeur->setFont(QFont("Comic Sans MS",25));
    etatServeur->show();

    QPushButton *boutonLancerJeux = new QPushButton();
    boutonLancerJeux->setText("Lancer Partie");
    listWidget.push_back(boutonLancerJeux);
    listBouton.push_back(boutonLancerJeux);
    connect(boutonLancerJeux,SIGNAL(clicked()),this,SLOT(lancerPartieLocal()));

    boutonRetour = new QPushButton();
    boutonRetour->setText("Retour");
    listWidget.push_back(etatServeur);
    listWidget.push_back(boutonRetour);
    gridLayout->addWidget(etatServeur,0,0);
    gridLayout->addLayout(gridLayout2,1,0);
    gridLayout->addWidget(boutonLancerJeux,2,0);
    gridLayout->addWidget(boutonRetour,2,1);
    setLayout(gridLayout);

    serveur = new QTcpServer(this);
    if (!serveur->listen(QHostAddress::Any, 2332)) // Démarrage du serveur sur toutes les IP disponibles et sur le port 2332
    {
        // Si le serveur n'a pas été démarré correctement
        etatServeur->setText(tr("Le serveur n'a pas pu être démarré. Raison :<br />") + serveur->errorString());
    }
    else
    {
        // Si le serveur a été démarré correctement
        etatServeur->setText(tr("Le serveur a été démarré sur le port <strong>") + QString::number(serveur->serverPort()) + tr("</strong>.<br />Des clients peuvent maintenant se connecter."));
        connect(serveur, SIGNAL(newConnection()), this, SLOT(nouvelleConnexion()));
    }

    host=true;
    client=false;
    tailleMessage = 0;
}

void MonMenu::lancerPartieLocal()
{
    if (partieLance==false)
    {
        host=true;
        client=false;
        QString messageAEnvoyer;
        int nombreJoueur = listClients.length()+1;
        messageAEnvoyer+="$01";
        ajouterIntAString(&messageAEnvoyer,nombreJoueur,2);
        ajouterIntAString(&messageAEnvoyer,pseudo.length(),2);
        messageAEnvoyer+=pseudo;
        qDebug() << "lancerPartieLocal - 1";

        for (int i = 0 ; i<listPseudo.length() ; i++ )
        {
            ajouterIntAString(&messageAEnvoyer,listPseudo[i]->length(),2);
            messageAEnvoyer+=*listPseudo[i];
        }
        messageAEnvoyer+='!';
        envoyerMessageATous(messageAEnvoyer);
        enAttenteDeJoueur=false;
        partieLance=true;

        for (int i=0;i<nombreJoueur;i++)
        {
            Personnage *personnage = new Personnage(this,i+1);
            listPersonnage.push_back(personnage);
        }
        partieLocal=true;
        host=true;
        for (int i = 0 ; i<listBouton.length();i++)
        {
            if(listBouton[i]!=NULL)
            {
                delete listBouton[i];
                listBouton[i]=NULL;
            }
        }
        clearListMenu();
        constructeur();
        jeuStandard();
    }
}

void MonMenu::nouvelleConnexion()
{
    //envoyerATous(tr("<em>Un nouveau client vient de se connecter</em>"));

    QTcpSocket *nouveauClient = serveur->nextPendingConnection();
    listClients << nouveauClient;


    nombreClient++;
    QString message;
    message = '$';
    message+=intToString(nombreClient);
    message+='!';
    envoieMessageClient(message,nouveauClient);
    connect(nouveauClient, SIGNAL(readyRead()), this, SLOT(donneesRecuesServeur()));
    connect(nouveauClient, SIGNAL(disconnected()), this, SLOT(deconnexionClient()));
}

void MonMenu::deconnexionClient()
{
    //envoyerATous(tr("<em>Un client vient de se déconnecter</em>"));

    // On détermine quel client se déconnecte
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket == 0) // Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
        return;
    int i = listClients.indexOf(socket);
    listPseudo.removeAt(i);
    listClients.removeOne(socket);

    socket->deleteLater();
}

void MonMenu::rejoindrePartieLocal()
{
    /*if (boutonRetour!=NULL)
    {
        delete boutonRetour;
        boutonRetour=NULL;
    }*/
    for (int i = 0 ; i<listBouton.length();i++)
    {
        delete listBouton[i];
        listBouton[i]=NULL;
    }
    for (int i = 0 ; i<listWidget.length();i++)
    {
        delete listWidget[i];
    }
    listWidget.clear();
    listLabel.clear();
    listLineEdit.clear();
    listBouton.clear();
    for (int i = 0; i< listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
    }
    listGridLayoutTouche.clear();
    QGridLayout *gridLayout = new QGridLayout;
    listGridLayoutTouche.push_back(gridLayout);
    QLabel *labelAdresseIP = new QLabel;
    labelAdresseIP->setText("Adresse IP serveur : ");
    QLabel *labelPort = new QLabel;
    labelPort->setText("Port du serveur : ");
    QLabel *labelPseudo = new QLabel();
    listLabel.push_back(labelPseudo);
    labelPseudo->setText("votre speudo :");
    QLineEdit * lineEditPseudo = new QLineEdit();
    lineEditPseudo->setText("pseudo");
    listLineEdit.push_back(lineEditPseudo);
    lineEditPseudo->setMaxLength(20);
    QPushButton *boutonConnexion = new QPushButton;
    boutonConnexion->setText("Connexion ");
    QLineEdit *lineEditIP = new QLineEdit;
    lineEditIP->setText("127.0.0.1");
    QSpinBox *spinBoxPort = new QSpinBox;
    spinBoxPort->setMaximum(65536);
    spinBoxPort->setValue(2332);
    gridLayout->addWidget(labelAdresseIP,0,0);
    gridLayout->addWidget(lineEditIP,0,1);
    gridLayout->addWidget(labelPort,1,0);
    gridLayout->addWidget(spinBoxPort,1,1);
    gridLayout->addWidget(boutonConnexion,0,2,2,1);
    gridLayout->addWidget(labelPseudo,2,0);
    gridLayout->addWidget(lineEditPseudo,2,1);
    setLayout(gridLayout);
    listWidget.push_back(lineEditIP);
    listWidget.push_back(spinBoxPort);
    listLineEdit.push_back(lineEditIP);
    listSpinBox.push_back(spinBoxPort);
    listWidget.push_back(boutonConnexion);
    listWidget.push_back(labelAdresseIP);
    listWidget.push_back(labelPseudo);
    listWidget.push_back(lineEditPseudo);
    listWidget.push_back(labelPort);
    connect(boutonConnexion,SIGNAL(clicked()),this,SLOT(connexion()));

}

void MonMenu::connexion()
{
    if (listLineEdit[0]->text()!="")
    {
        pseudo=listLineEdit[0]->text();
        socketClient = new QTcpSocket(this);
        connect(socketClient,SIGNAL(connected()),this,SLOT(connexion_OK()));
        connect(socketClient,SIGNAL(readyRead()),this,SLOT(donneesRecuesClient()));
        socketClient->connectToHost(listLineEdit[1]->text(),listSpinBox[0]->value());

    }

}

void MonMenu::connexion_OK()
{
    for (int i = 0 ; i<listWidget.length();i++)
    {
        delete listWidget[i];
    }
    listWidget.clear();
    listLabel.clear();
    listLineEdit.clear();
    listBouton.clear();
    listSpinBox.clear();
    for (int i = 0; i< listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
    }
    listGridLayoutTouche.clear();
    QGridLayout *gridLayout = new QGridLayout();
    listGridLayoutTouche.push_back(gridLayout);
    QLabel *labelMessage = new QLabel();
    labelMessage->setFont(QFont("Comic Sans MS",22));
    labelMessage->setText("vous êtes bien connecté au serveur. En attente de l'host");
    labelMessage->setAlignment(Qt::AlignTop);

    gridLayout->addWidget(labelMessage);
    setLayout(gridLayout);
    host=false;
    client=true;
    enAttenteIdentifiant=true;
}

void MonMenu::envoyerMessageATous(QString message)
{
    if (message[0]!='$')message.insert(0,'$');
    if (message[message.length()-1]!='!')message.insert(message.length(),'!');
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);
    QString messageAEnvoyer = message;

    out << messageAEnvoyer;
    qDebug() << messageAEnvoyer;
    for(int i = 0 ; i<listClients.length() ; i++)
    {
        listClients[i]->write(paquet); // On envoie le paquet
    }
}

void MonMenu::envoieMessageClient(QString message,QTcpSocket * socket)
{
    if (socket->isOpen())
    {
        if (message[0]!='$')message.insert(0,'$');
        if (message[message.length()-1]!='!')message.insert(message.length(),'!');
        QByteArray paquet;
        QDataStream out(&paquet, QIODevice::WriteOnly);
        QString messageAEnvoyer = message;

        out << messageAEnvoyer;

        socket->write(paquet); // On envoie le paquet
    }
    else qDebug() << "Vous êtes connectez à aucun serveur";
}

void MonMenu::envoieMessageServeur(QString message)
{
    if (socketClient->isOpen() && socketClient->isWritable())
    {
        //if ((message[0]=='0' && message[1]=='3')||(message[1]=='0' && message[2]=='3'))clientAtenteReponseDeplacement=true;
        if (clientAtenteReponseDeplacement==true && message[4]!='0' && message[0]=='0' && message[1]=='3')return;
        if (message[0]!='$')message.insert(0,'$');
        if (message[message.length()-1]!='!')message.insert(message.length(),'!');
        QByteArray paquet;
        QDataStream out(&paquet, QIODevice::WriteOnly);
        QString messageAEnvoyer = message;

        out << messageAEnvoyer;
        socketClient->write(paquet); // On envoie le paquet
    }
    else if (serveur->isListening()==false)
    {

    }
}

void MonMenu::donneesRecuesClient()
{
    while (socketClient->bytesAvailable()!=0)
    {
        QDataStream in(socketClient);
        QString messageTemp = "";
        in >> messageTemp;

        messageRecu += messageTemp;
    }
    for(int i = 0 ; i < messageRecu.length();i++)
    {
        if (messageRecu[i]=='!')nbMessageATraite++;
    }

    if (messageRecu.contains('!'))
    {

            if (nbMessageATraite>1)
            {
                qDebug() <<messageRecu;
            }
            int index=messageRecu.indexOf('!');
            messageRecu.remove(index,1);
            index=messageRecu.indexOf('$');
            messageRecu.remove(index,1);
            while(messageRecu.indexOf(' ')!=-1)
            {
                index=messageRecu.indexOf(' ');
                messageRecu.remove(index,1);
            }
            while(messageRecu.indexOf('\'')!=-1)
            {
                index=messageRecu.indexOf('\'');
                messageRecu.remove(index,1);
            }
            while (nbMessageATraite>0)
            {
            if (enAttenteIdentifiant==true)
            {
                numeroClient=(messageRecu.toInt());
                enAttenteIdentifiant=false;
                QString messageAEnvoyer="$00";
                ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                messageAEnvoyer+=pseudo;
                messageAEnvoyer+='!';
                envoieMessageServeur(messageAEnvoyer);
                enAttenteIdentifiant=false;
            }

            if (messageRecu[0]=='0' && messageRecu[1]=='1')
            {
                messageRecu.remove(0,2);
                QString string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                nombreJoueur = string.toInt();

                messageRecu.remove(0,2);
                int taillePseudo=0;
                for (int i=0 ; i<nombreJoueur ; i++)
                {
                    string.clear();
                    string="";
                    string+=messageRecu[0];
                    string+=messageRecu[1];
                    taillePseudo=string.toInt();
                    messageRecu.remove(0,2);
                    string.clear();
                    string="";
                    for (int y = 0 ; y<taillePseudo ; y++)
                    {
                        string+=messageRecu[y];
                    }
                    if(pseudo!=string)
                    {
                        QString *pseudo = new QString();
                        *pseudo=string;
                        listPseudo.push_back(pseudo);

                    }
                    messageRecu.remove(0,taillePseudo);

                }
                for (int i = 0; i <listPseudo.length();i++)
                {
                }
                for (int i=0;i<nombreJoueur;i++)
                {
                    Personnage *personnage = new Personnage(this,i+1);
                    listPersonnage.push_back(personnage);
                }
                boutonRetour=NULL;
                clearListMenu();
                constructeur();
                partieLocal=true;
                clientAtenteReponseDeplacement=false;
                jeuStandard();

            }
            else if (messageRecu[0]=='0' && messageRecu[1]=='3')
            {
                clientAtenteReponseDeplacement=false;
                messageRecu.remove(0,2);
                int index=0;
                QString string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                index=string.toInt();
                messageRecu.remove(0,2);
                bool deplacement;
                if (messageRecu[0]=='0')deplacement=false;
                if (messageRecu[0]=='1')deplacement=true;
                messageRecu.remove(0,1);
                int direction=5;

                direction=messageRecu[0].toLatin1()-0x30;

                if(index<0 || index> listPersonnage.length()-1)return;
                if(direction==0)listPersonnage[index]->deplacementBas=deplacement;
                if(direction==1)listPersonnage[index]->deplacementHaut=deplacement;
                if(direction==2)listPersonnage[index]->deplacementGauche=deplacement;
                if(direction==3)listPersonnage[index]->deplacementDroite=deplacement;

                messageRecu.remove(0,1);
                int positionX;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionX=string.toInt();
                messageRecu.remove(0,3);

                int positionY;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionY=string.toInt();

                listPersonnage[index]->setPosition(positionX,positionY);
            }
            else if (messageRecu[0]=='0' && messageRecu[1]=='4')
            {
                messageRecu.remove(0,2);
                int index=0;
                QString string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                index=string.toInt();
                messageRecu.remove(0,2);
                int positionX;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionX=string.toInt();
                messageRecu.remove(0,3);

                int positionY;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionY=string.toInt();
                listPersonnage[index]->setPosition(positionX,positionY);

                emit poserBombeSignal(listPersonnage[index]);

            }
            else if (messageRecu[0]=='0' && messageRecu[1]=='5')
            {
                messageRecu.remove(0,2);
                int nombreAleat;
                QString string("");
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                nombreAleat=string.toInt();
                messageRecu.remove(0,3);
                Coord coordonee;
                int positionX;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionX=string.toInt();
                messageRecu.remove(0,3);

                int positionY;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                string+=messageRecu[2];
                positionY=string.toInt();

                coordonee.x=positionX;
                coordonee.y=positionY;

                Bonus *bonus = new Bonus(this,nombreAleat,coordonee);
                listElementMapSensibleFlamme.push_back(bonus);

            }
            nbMessageATraite--;
            if(nbMessageATraite>0)
            {
                int index2;
                index2=messageRecu.indexOf('$');
                messageRecu.remove(0,index2+1);// on supprime le $
            }
            else  if (messageRecu.contains('$'))
            {
                int index2=messageRecu.indexOf('$');
                messageRecu.remove(0,index2);// on supprime pas le $
            }
            else
            {
                messageRecu="";
                nbMessageATraite=0;
            }
        }
    }
}

void MonMenu::donneesRecuesServeur()
{

    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    if (socket == 0)return;// Si par hasard on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
    while (socket->bytesAvailable()!=0)
    {
        QDataStream in(socket);
        QString messageTemp = "";
        in >> messageTemp;

        messageRecu += messageTemp;
    }
    for(int i = 0 ; i < messageRecu.length();i++)
    {
        if (messageRecu[i]=='!')nbMessageATraite++;
    }

    if (messageRecu.contains('!'))
    {
        if (nbMessageATraite>1)
        {
            qDebug() <<messageRecu;
        }
        int index=messageRecu.indexOf('!');
        //messageRecu.remove(index,messageRecu.length());
        messageRecu.remove(index,1);
        index=messageRecu.indexOf('$');
        //messageRecu.remove(index,messageRecu.length());
        messageRecu.remove(index,1);
        while(messageRecu.indexOf(' ')!=-1)
            {
                index=messageRecu.indexOf(' ');
                messageRecu.remove(index,1);
            }
        while(messageRecu.indexOf('\'')!=-1)
            {
                index=messageRecu.indexOf('\'');
                messageRecu.remove(index,1);
            }

        // traitement pseudo
        while(nbMessageATraite!=0)
        {

            if (messageRecu[0]=='0' && messageRecu[1]=='0' && enAttenteDeJoueur==true)
            {
                QString stringIndex="";
                stringIndex+=messageRecu[2];
                stringIndex+=messageRecu[3];
                int index = stringIndex.toInt()-1;
                messageRecu.remove(0,4);
                QString *pseudo = new QString();
                *pseudo=messageRecu;
                listPseudo.push_back(pseudo);
                QLabel *label = new QLabel();
                label->setFont(QFont("Comic Sans MS",20));
                label->setText(*listPseudo[index]);
                listLabel.push_back(label);
                listWidget.push_back(label);
                listGridLayoutTouche[1]->addWidget(label);
            }

            // fin traitement pseudo

            else if (messageRecu[0]=='0' && messageRecu[1]=='3')
            {
                int index=0;
                int indexFinMessage;
                if (nbMessageATraite>1)indexFinMessage=messageRecu.indexOf('!');
                else indexFinMessage=messageRecu.length();
                QString string="";
                string+=messageRecu[2];
                string+=messageRecu[3];
                index=string.toInt();
                QString position="";

                ajouterIntAString(&position,listPersonnage[index]->position.y,3);
                messageRecu.insert(indexFinMessage,QString(position));

                position="";

                ajouterIntAString(&position,listPersonnage[index]->position.x,3);
                messageRecu.insert(indexFinMessage,QString(position));

                position="";
                qDebug() << messageRecu;

                envoyerMessageATous(messageRecu);
                messageRecu.remove(0,2);
                index=0;
                string="";
                string+=messageRecu[0];
                string+=messageRecu[1];
                index=string.toInt();
                messageRecu.remove(0,2);
                bool deplacement;
                if (messageRecu[0]=='0')deplacement=false;
                else if (messageRecu[0]=='1')deplacement=true;
                messageRecu.remove(0,1);
                int direction=5;
                direction=messageRecu[0].toLatin1()-0x30;
                if (index<0 || index>listPersonnage.length()-1)return;
                if(direction==0)listPersonnage[index]->deplacementBas=deplacement;
                if(direction==1)listPersonnage[index]->deplacementHaut=deplacement;
                if(direction==2)listPersonnage[index]->deplacementGauche=deplacement;
                if(direction==3)listPersonnage[index]->deplacementDroite=deplacement;
            }

            else if (messageRecu[0]=='0' && messageRecu[1]=='4')
            {
                int index=0;
                int indexFinMessage;
                if (nbMessageATraite>1)indexFinMessage=messageRecu.indexOf('!');
                else indexFinMessage=messageRecu.length();
                QString string="";
                string+=messageRecu[2];
                string+=messageRecu[3];
                index=string.toInt();
                QString position="";

                ajouterIntAString(&position,listPersonnage[index]->position.y,3);
                messageRecu.insert(indexFinMessage,QString(position));

                position="";

                ajouterIntAString(&position,listPersonnage[index]->position.x,3);
                messageRecu.insert(indexFinMessage,QString(position));

                position="";
                qDebug() << messageRecu;
                envoyerMessageATous(messageRecu);
                emit poserBombeSignal(listPersonnage[index]);
            }
            nbMessageATraite--;
            if(nbMessageATraite>0)
            {
                int index2;
                index2=messageRecu.indexOf('$');
                messageRecu.remove(0,index2+1);// on supprime le $
            }
            else  if (messageRecu.contains('$'))
            {
                int index2=messageRecu.indexOf('$');
                messageRecu.remove(0,index2);// on supprime pas le $
            }
            else
            {
                messageRecu="";
                nbMessageATraite=0;
            }

        }
    }

}

void MonMenu::clearListMenu()
{
    /*for(int i = 0 ; i<listWidget.length();i++)
    {
        delete listWidget[i];
        listWidget[i]=NULL;
    }*/
    listWidget.clear();
    listLabel.clear();
    listLineEdit.clear();
    listSpinBox.clear();

    /*for (int i = 0 ; listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
        listGridLayoutTouche[i]=NULL;
    }*/
    listGridLayoutTouche.clear();

}

void MonMenu::retirerChoixJeuxLocal()
{
    for (int i = 0; i< listBouton.length();i++)
    {
        delete listBouton[i];
    }
    listBouton.clear();
    for (int i = 0; i< listGridLayoutTouche.length();i++)
    {
        delete listGridLayoutTouche[i];
    }
    listGridLayoutTouche.clear();
    delete boutonRetour;
    boutonRetour = NULL;
    constructeur();
}

void MonMenu::retirerChoixMultijoueur()
{
    if (multiEnLigne!=NULL)
    {
        delete multiEnLigne;
        multiEnLigne=NULL;
    }
    if (multiLocal!=NULL)
    {
        delete multiLocal;
        multiLocal=NULL;
    }
    if (boutonRetour!=NULL)
    {
        delete boutonRetour;
        boutonRetour=NULL;
    }

    connect(boutonMultiJoueur,SIGNAL(clicked()),this, SLOT(choixMultijoueur()));
}

void MonMenu::ajoutJoueur()
{
    int numeroJoueur=0;
    for (int i = 0; i<listPersonnage.length();i++ )
    {
        delete listPersonnage[i];
    }
    listPersonnage.clear();

    while (listPersonnage.length()<spinBoxNombreJoueur->value())
    {
        numeroJoueur = spinBoxNombreJoueur->value() - (spinBoxNombreJoueur->value() - listPersonnage.length())+1;
        joueur1 = new Personnage(this,numeroJoueur);
        listPersonnage.push_back(joueur1);
    }

    int compt=0;
    int ligne=0;
    QLayoutItem *child;
    if (!listGridLayoutTouche.isEmpty())
    {
        for(int i = 0;i<listGridLayoutTouche.length();i++)
        {
            while ((child = listGridLayoutTouche[i]->takeAt(0)) != 0)
            {
                child->widget()->deleteLater();
                delete child;
            }
        }
    }

    listGridLayoutTouche.clear();
    for(int i = 0;i<listPersonnage.length();i++)
    {
        QGridLayout *gridLayoutTouche = new QGridLayout();
        listGridLayoutTouche.push_back(gridLayoutTouche);
        for(int y = 0; y<Personnage::nbToucheUtile;y++)
        {

            BoutonTouche *bouton = new BoutonTouche(this,i);
            if (listPersonnage.length()<=4)bouton->setMaximumWidth((resolutionTaille.x/sqrt(listPersonnage.length()))/4.5);
            else if (listPersonnage.length()<=6)bouton->setMaximumWidth((resolutionTaille.x/(sqrt(listPersonnage.length())+1))/4.5);
            connect(bouton,SIGNAL(toucheChoisie(int,int,int)),this,SLOT(clickBoutonConfigurationTouche(int,int,int)));

            if(y==0)bouton->setText("Haut");
            if(y==1)bouton->setText("Bas");
            if(y==2)bouton->setText("Gauche");
            if(y==3)bouton->setText("Droite");
            if(y==4)bouton->setText("Poser Bombe");
            if(y==5)bouton->setText("Bouton Action 1");
            listWidgetTouche.push_back(bouton);
            if (y<=3)
            {
                if(y==0)gridLayoutTouche->addWidget(bouton,0,1);
                else if(y==1)gridLayoutTouche->addWidget(bouton,2,1);
                else if(y==2)gridLayoutTouche->addWidget(bouton,1,0);
                else if(y==3)gridLayoutTouche->addWidget(bouton,1,2);
            }
            else
            {
                gridLayoutTouche->addWidget(bouton,y-4,3);
            }
            gridLayoutTouche->setAlignment(bouton,Qt::AlignTop);
        }

        gridLayoutControle->addLayout(gridLayoutTouche,ligne+1,compt);

        BoutonTouche::nbBouton=0;
        compt++;
        if(compt>=sqrt(listPersonnage.length()))
        {
            compt=0;
            ligne++;
        }

    }

    this->setLayout(gridLayoutControle);

}

void MonMenu::retirerChoixConfigurationTouche()
{
    QLayoutItem *child;
    for(int i = 0;i<listGridLayoutTouche.length();i++)
    {
        while ((child = listGridLayoutTouche[i]->takeAt(0)) != 0)
        {
            child->widget()->deleteLater();
            delete child;
        }
    }
    listGridLayoutTouche.clear();
    boutonRetour=NULL;

    while ((child = LayoutMenuConfigurationToucheNombreJoueur->takeAt(0)) != 0)
    {
        child->widget()->deleteLater();
        delete child;
    }

    constructeur();
}

void MonMenu::clickBoutonConfigurationTouche(int touche,int joueurConfigurationTouche,int numeroBoutonConfiguration)
{
    if(numeroBoutonConfiguration==0)
    {
        listPersonnage[joueurConfigurationTouche]->toucheHaut=touche;
    }
    if(numeroBoutonConfiguration==1)
    {
        listPersonnage[joueurConfigurationTouche]->toucheBas=touche;
    }
    if(numeroBoutonConfiguration==2)
    {
        listPersonnage[joueurConfigurationTouche]->toucheGauche=touche;
    }
    if(numeroBoutonConfiguration==3)
    {
        listPersonnage[joueurConfigurationTouche]->toucheDroite=touche;
    }
    if(numeroBoutonConfiguration==4)
    {
        listPersonnage[joueurConfigurationTouche]->touchePoserBombe=touche;
    }
    if(numeroBoutonConfiguration==5)
    {
        listPersonnage[joueurConfigurationTouche]->toucheAction1=touche;
    }
}

void MonMenu::retirerChoixOption()
{
    if (boutonConfigurationTouche!=NULL && boutonRetour!=NULL)
    {
        layoutOption->removeWidget(boutonConfigurationTouche);
        layoutOption->removeWidget(boutonRetour);
        delete boutonRetour;
        delete boutonConfigurationTouche;
        boutonRetour=NULL;
        boutonConfigurationTouche=NULL;
        connect(boutonOption,SIGNAL(clicked()),this,SLOT(choixOption()));
    }
    appuiToucheConfiguration = false;
}

void MonMenu::jeuStandard()
{
    destructeur();
    boutonResolutionActive=false;
    boutonResolution1=NULL;
    boutonResolution2=NULL;
    boutonRetour=NULL;
    boutonJeuPersonnalise=NULL;
    boutonJeuStandard=NULL;

    objetBouclier=false;
    objetLanceBombe=false;
    objetMalus=true;
    objetPortail=false;
    objetPousserBombe=true;

    nombreBlockX=17;
    nombreBlockY=13;
    tailleBlockX=(resolutionTaille.x-((resolutionTaille.x*0.15)))/nombreBlockX;
    tailleBlockY=(resolutionTaille.y-((resolutionTaille.y*0.15)))/nombreBlockY;
    creationTerrain();
    this->show();
}

void MonMenu::creationTerrain()
{
    int i,j;
    srand(time(NULL));
    for (i=0;i<nombreBlockX;i++)
    {

        for(j=0;j<nombreBlockY;j++)
        {
            Sol *sol = new Sol(this);
            sol->setTaille(tailleBlockX,tailleBlockY);
            sol->setPosition(tailleBlockX*i,tailleBlockY*j);

            sol->setImage("../decor/sol.png");
            sol->miseAJour();
            listSol.push_back(sol);
            listElementMap.push_back(sol);

        }
    }
    for (i=0;i<nombreBlockX;i++)
    {

        for(j=0;j<nombreBlockY;j++)
        {
            if (j==0 || j==nombreBlockY-1 || i==0 || i==nombreBlockX-1)
            {
                MurIncassable *murIncassable = new MurIncassable(this);
                murIncassable->setTaille(tailleBlockX,tailleBlockY);
                murIncassable->setPosition(tailleBlockX*i,tailleBlockY*j);

                murIncassable->setImage("../decor/murIncassable.png");
                murIncassable->miseAJour();

                listMurIncassable.push_back(murIncassable);
                listElementMap.push_back(murIncassable);
                listElementMapInfranchissable.push_back(murIncassable);

            }
            else if ( !((j==1 && (i==2 ||i==1 || i==nombreBlockX-3 || i==nombreBlockX-2))
                   || (j==2 && i==1) || (j==2 && i==nombreBlockX-2)
                   || (j==nombreBlockY-2 && (i== nombreBlockX-2 || i== nombreBlockX-3 || i==1 || i==2))
                   ||(j==nombreBlockY-3 && i==1) || j==nombreBlockY-3 && i==nombreBlockX-2
                   )
                 )
            {
                if (j%2!=0)//donc si j est impaire, donc ligne sans blocks incassable
                {
                    int nombreAleat = (rand() % (3 - 0)) ;
                    nombreAleat=1;
                    if(nombreAleat==1 || nombreAleat==2 || nombreAleat == 3)
                    {

                        Mur *mur=new Mur(this);
                        mur->setTaille(tailleBlockX,tailleBlockY);
                        mur->setPosition(tailleBlockX*(i),tailleBlockY*j);

                        mur->setImage("../decor/mur.png");
                        mur->miseAJour();

                        listMur.push_back(mur);
                        listElementMap.push_back(mur);
                        listElementMapInfranchissable.push_back(mur);
                        listElementMapSensibleFlamme.push_back(mur);
                    }
                }
                else
                {
                    if (i%2==0)
                    {
                        MurIncassable *murIncassable = new MurIncassable(this);
                        murIncassable->setTaille(tailleBlockX,tailleBlockY);
                        murIncassable->setPosition(tailleBlockX*i,tailleBlockY*j);

                        murIncassable->setImage("../decor/murIncassable.png");
                        murIncassable->miseAJour();

                        listMurIncassable.push_back(murIncassable);
                        listElementMap.push_back(murIncassable);
                        listElementMapInfranchissable.push_back(murIncassable);
                    }
                    else
                    {
                        int nombreAleat = (rand() % (2 - 0)) ;
                        nombreAleat = 1 ;
                        if(nombreAleat==1 || nombreAleat==2)
                        {

                            Mur *mur=new Mur(this);
                            mur->setTaille(tailleBlockX,tailleBlockY);
                            mur->setPosition(tailleBlockX*i,tailleBlockY*j);

                            mur->setImage("../decor/mur.png");
                            mur->miseAJour();

                            listMur.push_back(mur);
                            listElementMap.push_back(mur);
                            listElementMapInfranchissable.push_back(mur);
                            listElementMapSensibleFlamme.push_back(mur);
                        }
                    }
                }
            }
        }
    }
    for (i=0;i<listPersonnage.length();i++)
    {
        Personnage *personnage = new Personnage(this,i+1);
        personnage->toucheBas=listPersonnage[i]->toucheBas;
        personnage->toucheHaut=listPersonnage[i]->toucheHaut;
        personnage->toucheGauche=listPersonnage[i]->toucheGauche;
        personnage->toucheDroite=listPersonnage[i]->toucheDroite;
        personnage->touchePoserBombe=listPersonnage[i]->touchePoserBombe;
        listPersonnage[i]=personnage;
        if(i==0)
        {
            if (host==true)
            {
                listPersonnage[i]->jouable=true;
            }
            listPersonnage[i]->setTaille(tailleBlockX/1.3,tailleBlockY/1.3);
            listPersonnage[i]->setPosition(tailleBlockX,tailleBlockY);

            listPersonnage[i]->setImage("../personnage/personnage1/bas.png");
            listPersonnage[i]->miseAJour();

        }
        if(i==1)
        {
            listPersonnage[i]->setTaille(tailleBlockX/1.3,tailleBlockY/1.3);
            listPersonnage[i]->setPosition(nombreBlockX*tailleBlockX-2*tailleBlockX,nombreBlockY*tailleBlockY-2*tailleBlockY);

            listPersonnage[i]->setImage("../personnage/personnage2/bas.png");
            listPersonnage[i]->miseAJour();
        }
        if(i==2)
        {
            listPersonnage[i]->setTaille(tailleBlockX/1.3,tailleBlockY/1.3);
            listPersonnage[i]->setPosition(nombreBlockX*tailleBlockX-2*tailleBlockX,tailleBlockY);

            listPersonnage[i]->setImage("../personnage/personnage3/bas.png");
            listPersonnage[i]->miseAJour();
        }
        if(i==3)
        {
            listPersonnage[i]->setTaille(tailleBlockX/1.3,tailleBlockY/1.3);
            listPersonnage[i]->setPosition(tailleBlockX,nombreBlockY*tailleBlockY-2*tailleBlockY);

            listPersonnage[i]->setImage("../personnage/personnage4/bas.png");
            listPersonnage[i]->miseAJour();

        }
        listPersonnage[i]->setFocus();
        listPersonnage[i]->clearFocus();

        listElementMap.push_back(listPersonnage[i]);
        listElementMapSensibleFlamme.push_back(listPersonnage[i]);
        if (partieLocal==true)
        {
            if (host==true)
            {
                if (i==0)personnage->jouable=true;
                else personnage->jouable=false;
            }
            else if (client==true)
            {
                if (i==numeroClient)personnage->jouable=true;
                else personnage->jouable=false;
            }
        }
        else
        {
            personnage->jouable=true;
        }
    }
    if(listPersonnage.length()==0)
    {
        for (int i=0;i<2;i++)
        {
            Personnage *personnage = new Personnage(this,i+1);
            listPersonnage.push_back(personnage);
            listPersonnage[i]->setTaille(tailleBlockX/1.1,tailleBlockY/1.1);
            if(i==0)
            {

                listPersonnage[i]->setPosition(tailleBlockX+9,tailleBlockY-20);

                listPersonnage[i]->setImage("../personnage/personnage1/bas.png");
                listPersonnage[i]->miseAJour();

            }
            if(i==1)
            {
                listPersonnage[i]->setPosition(nombreBlockX*tailleBlockX-2*tailleBlockX,nombreBlockY*tailleBlockY-2*tailleBlockY);

                listPersonnage[i]->setImage("../personnage/personnage2/bas.png");
                listPersonnage[i]->miseAJour();
            }
            listPersonnage[i]->setFocus();
            listPersonnage[i]->clearFocus();

            //listPersonnage[i]->miseAJour();
            listElementMap.push_back(listPersonnage[i]);
            listElementMapSensibleFlamme.push_back(listPersonnage[i]);

        }

    }

    deplacementJoueurConnecte=true;
    setFocus();
}

bool MonMenu::event(QEvent *eventTemp)
{
    if (eventTemp->type() == QEvent::KeyRelease)
        {

            ke = static_cast<QKeyEvent *>(eventTemp);
            if(ke->isAutoRepeat() )
            {
                //ke->ignore();
                return true;
            }
            if (deplacementJoueurConnecte==true)
            {
                for(int i = 0; i< listPersonnage.length();i++)
                {
                    if (ke->key() == listPersonnage[i]->toucheBas && listPersonnage[i]->jouable==true)
                    {
                        if(partieLocal==true && paquetDeplacementBasEnvoye==true)
                        {
                            paquetDeplacementBasEnvoye=false;
                            if (host==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="030000";
                                QString position;
                                ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                messageAEnvoyer+=position;
                                position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                messageAEnvoyer+=position;
                                envoyerMessageATous(messageAEnvoyer);
                                listPersonnage[i]->deplacementBas=false;
                            }
                            else if (client==true)
                            {

                                QString messageAEnvoyer="";
                                messageAEnvoyer+="03";
                                ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                messageAEnvoyer+="00";
                                envoieMessageServeur(messageAEnvoyer);
                            }
                        }
                        else if(partieLocal==false) listPersonnage[i]->deplacementBas=false;
                        //listPersonnage[i]->miseAJour();
                        if(partieLocal==false)
                        {
                            /*if (listPersonnage[i]->deplacementBas==false && listPersonnage[i]->deplacementDroite==false && listPersonnage[i]->deplacementGauche==false && listPersonnage[i]->deplacementHaut==false)
                            {
                                disconnect(listPersonnage[i]->timer,SIGNAL(timeout()),listPersonnage[i],SLOT(deplacerJoueur()));
                                deplacementJoueurConnecte=false;
                            }*/
                        }
                        return true;
                    }
                    else if (ke->key() == listPersonnage[i]->toucheHaut && listPersonnage[i]->jouable==true)
                    {
                        if(partieLocal==true && paquetDeplacementHautEnvoye==true)
                        {
                            paquetDeplacementHautEnvoye=false;
                            if (host==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="030001";
                                QString position;
                                ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                messageAEnvoyer+=position;
                                position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                messageAEnvoyer+=position;
                                envoyerMessageATous(messageAEnvoyer);
                                listPersonnage[i]->deplacementHaut=false;
                            }
                            else if (client==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="03";
                                ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                messageAEnvoyer+="01";
                                envoieMessageServeur(messageAEnvoyer);
                            }
                        }
                        else if(partieLocal==false) listPersonnage[i]->deplacementHaut=false;
                        //listPersonnage[i]->miseAJour();
                        if(partieLocal==false)
                        {
                            /*if (listPersonnage[i]->deplacementBas==false && listPersonnage[i]->deplacementDroite==false && listPersonnage[i]->deplacementGauche==false && listPersonnage[i]->deplacementHaut==false)
                            {
                                disconnect(listPersonnage[i]->timer,SIGNAL(timeout()),listPersonnage[i],SLOT(deplacerJoueur()));
                                deplacementJoueurConnecte=false;
                            }*/
                        }
                        return true;
                    }
                    else if (ke->key() == listPersonnage[i]->toucheDroite && listPersonnage[i]->jouable==true)
                    {
                        if(partieLocal==true && paquetDeplacementDroiteEnvoye==true)
                        {
                            paquetDeplacementDroiteEnvoye=false;
                            if (host==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="030003";
                                QString position;
                                ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                messageAEnvoyer+=position;
                                position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                messageAEnvoyer+=position;
                                listPersonnage[i]->deplacementDroite=false;
                                envoyerMessageATous(messageAEnvoyer);
                            }
                            else if (client==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="03";
                                ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                messageAEnvoyer+="03";
                                envoieMessageServeur(messageAEnvoyer);
                            }
                        }
                        else if(partieLocal==false) listPersonnage[i]->deplacementDroite=false;
                        //listPersonnage[i]->miseAJour();
                        if(partieLocal==false)
                        {
                            /*if (listPersonnage[i]->deplacementBas==false && listPersonnage[i]->deplacementDroite==false && listPersonnage[i]->deplacementGauche==false && listPersonnage[i]->deplacementHaut==false)
                            {
                                disconnect(listPersonnage[i]->timer,SIGNAL(timeout()),listPersonnage[i],SLOT(deplacerJoueur()));
                                deplacementJoueurConnecte=false;
                            }*/
                        }
                        return true;
                    }
                    else if (ke->key() == listPersonnage[i]->toucheGauche && listPersonnage[i]->jouable==true)
                    {
                        if(partieLocal==true && paquetDeplacementGaucheEnvoye==true)
                        {
                            paquetDeplacementGaucheEnvoye=false;
                            if (host==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="030002";
                                QString position;
                                ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                messageAEnvoyer+=position;
                                position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                messageAEnvoyer+=position;
                                envoyerMessageATous(messageAEnvoyer);
                                listPersonnage[i]->deplacementGauche=false;
                            }
                            else if (client==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="03";
                                ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                messageAEnvoyer+="02";
                                envoieMessageServeur(messageAEnvoyer);
                            }
                        }
                        else if(partieLocal==false) listPersonnage[i]->deplacementGauche=false;
                        //listPersonnage[i]->miseAJour();
                        if(partieLocal==false)
                        {
                            /*if (listPersonnage[i]->deplacementBas==false && listPersonnage[i]->deplacementDroite==false && listPersonnage[i]->deplacementGauche==false && listPersonnage[i]->deplacementHaut==false)
                            {
                                disconnect(listPersonnage[i]->timer,SIGNAL(timeout()),listPersonnage[i],SLOT(deplacerJoueur()));
                                deplacementJoueurConnecte=false;
                            }*/
                        }
                        return true;

                    }
                }
            }

        }

    if (eventTemp->type() == QEvent::KeyPress)
        {
        ke = static_cast<QKeyEvent *>(eventTemp);
        if(ke->isAutoRepeat() )
        {
            //ke->ignore();
            return true;
        }

            if (deplacementJoueurConnecte==true)
            {
                for(int i = 0;i<listPersonnage.length();i++)
                    {
                        if (ke->key() == listPersonnage[i]->toucheBas || ke->key() == listPersonnage[i]->toucheHaut
                            || ke->key() == listPersonnage[i]->toucheGauche || ke->key() == listPersonnage[i]->toucheDroite)
                        {
                            if (ke->key() == listPersonnage[i]->toucheBas && listPersonnage[i]->jouable==true)
                            {
                                if(partieLocal==true && paquetDeplacementBasEnvoye==false)
                                {
                                    paquetDeplacementBasEnvoye=true;
                                    if (host==true)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="030010";
                                        QString position;
                                        ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                        messageAEnvoyer+=position;
                                        position="";
                                        ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                        messageAEnvoyer+=position;
                                        envoyerMessageATous(messageAEnvoyer);
                                        listPersonnage[i]->deplacementBas=true;
                                    }
                                    else if (client==true && clientAtenteReponseDeplacement==false)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="03";
                                        ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                        messageAEnvoyer+="10";
                                        envoieMessageServeur(messageAEnvoyer);
                                        clientAtenteReponseDeplacement=true;
                                    }
                                }
                                else if(partieLocal==false) listPersonnage[i]->deplacementBas=true;
                                return true;

                            }
                            else if (ke->key() == listPersonnage[i]->toucheHaut && listPersonnage[i]->jouable==true)
                            {
                                if(partieLocal==true && paquetDeplacementHautEnvoye==false)
                                {
                                    paquetDeplacementHautEnvoye=true;
                                    if (host==true)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="030011";
                                        QString position;
                                        ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                        messageAEnvoyer+=position;
                                        position="";
                                        ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                        messageAEnvoyer+=position;
                                        envoyerMessageATous(messageAEnvoyer);
                                        listPersonnage[i]->deplacementHaut=true;
                                    }
                                    else if (client==true && clientAtenteReponseDeplacement==false)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="03";
                                        ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                        messageAEnvoyer+="11";
                                        envoieMessageServeur(messageAEnvoyer);
                                        clientAtenteReponseDeplacement=true;
                                    }
                                }
                                else if(partieLocal==false)listPersonnage[i]->deplacementHaut=true;
                                return true;
                            }
                            else if (ke->key() == listPersonnage[i]->toucheDroite && listPersonnage[i]->jouable==true)
                            {
                                if(partieLocal==true && paquetDeplacementDroiteEnvoye==false)
                                {
                                    paquetDeplacementDroiteEnvoye=true;
                                    if (host==true)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="030013";
                                        QString position;
                                        ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                        messageAEnvoyer+=position;
                                        position="";
                                        ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                        messageAEnvoyer+=position;
                                        envoyerMessageATous(messageAEnvoyer);
                                        listPersonnage[i]->deplacementDroite=true;
                                    }
                                    else if (client==true)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="03";
                                        ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                        messageAEnvoyer+="13";
                                        envoieMessageServeur(messageAEnvoyer);
                                        clientAtenteReponseDeplacement=true;
                                    }
                                }
                                else if(partieLocal==false) listPersonnage[i]->deplacementDroite=true;
                                return true;
                            }
                            else if (ke->key() == listPersonnage[i]->toucheGauche && listPersonnage[i]->jouable==true)
                            {
                                if(partieLocal==true && paquetDeplacementGaucheEnvoye==false)
                                {
                                    paquetDeplacementGaucheEnvoye=true;
                                    if (host==true)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="030012";
                                        QString position;
                                        ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                        messageAEnvoyer+=position;
                                        position="";
                                        ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                        messageAEnvoyer+=position;
                                        envoyerMessageATous(messageAEnvoyer);
                                        listPersonnage[i]->deplacementGauche=true;
                                    }
                                    else if (client==true && clientAtenteReponseDeplacement==false)
                                    {
                                        QString messageAEnvoyer="";
                                        messageAEnvoyer+="03";
                                        ajouterIntAString(&messageAEnvoyer,numeroClient,2);
                                        messageAEnvoyer+="12";
                                        envoieMessageServeur(messageAEnvoyer);
                                        clientAtenteReponseDeplacement=true;
                                    }
                                }
                                else if(partieLocal==false) listPersonnage[i]->deplacementGauche=true;
                                return true;
                            }

                        }
                    }

                for (int i = 0;i<listPersonnage.length();i++)
                {
                    if (ke->key() == listPersonnage[i]->touchePoserBombe && listPersonnage[i]->jouable==true)
                    {
                        if (partieLocal==true && listPersonnage[i]->jouable==true)
                        {
                            if (host==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="0400";
                                /*QString string="";
                                string = listPersonnage[i]->typeBombe+0x30;
                                messageAEnvoyer+=string;
                                string="";
                                ajouterIntAString(&string,listPersonnage[i]->flamme,2);
                                messageAEnvoyer+=string;
                                string="";*/
                                QString position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.x,3);
                                messageAEnvoyer+=position;
                                position="";
                                ajouterIntAString(&position,listPersonnage[i]->position.y,3);
                                messageAEnvoyer+=position;
                                envoyerMessageATous(messageAEnvoyer);
                                emit poserBombeSignal(listPersonnage[i]);
                            }
                            else if (client==true)
                            {
                                QString messageAEnvoyer="";
                                messageAEnvoyer+="04";
                                QString string="";
                                ajouterIntAString(&string,numeroClient,2);
                                messageAEnvoyer+=string;
                                string="";
                                /*
                                string = listPersonnage[i]->typeBombe+0x30;
                                messageAEnvoyer+=string;
                                string="";
                                ajouterIntAString(&string,listPersonnage[i]->flamme,2);
                                messageAEnvoyer+=string;*/
                                envoieMessageServeur(messageAEnvoyer);
                            }
                        }
                        else if (partieLocal==false && clientAtenteReponseDeplacement==false)
                        {
                            emit poserBombeSignal(listPersonnage[i]);
                        }

                    }
                    if (ke->key() == listPersonnage[i]->toucheAction1 && listPersonnage[i]->jouable==true)
                    {
                        emit signalExplosionBombe();
                        return true;
                    }

                }
            }
        }

    return QWidget::event(eventTemp);
}

void MonMenu::explosionBombe(Coord position,short int type,short int tailleFlamme,short int joueur)
{
    position.x=int((position.x/tailleBlockX))*tailleBlockX;
    position.y=int((position.y/tailleBlockY))*tailleBlockY;

    bool flammeDroite=true;
    bool flammeGauche=true;
    bool flammeHaut=true;
    bool flammeBas=true;
    bool poserBombeOK=true;

    if (type==1 ||type==3 || type==4)
    {
        int compt=0;
        Flamme *flamme = new Flamme(this);
        flamme->numeroFlamme=compteurNombreExplosionBombe;
        flamme->setTaille(tailleBlockX,tailleBlockY);
        flamme->setPosition(position.x-compt*tailleBlockX,position.y);
        flamme->setDirectionFlamme("centre");
        flamme->setImage("../flamme/flamme1/centre");
        listFlamme.push_back(flamme);
        flamme->miseAJour();
        for(int i=0;i<listPersonnage.length();i++)
        {
            if((listPersonnage[i]->position.x/tailleBlockX)*tailleBlockX==position.x&&
                    ((listPersonnage[i]->position.y+tailleBlockY/2+1)/tailleBlockY)*tailleBlockY==position.y)
            {
                emit elementToucheFlamme(listPersonnage[i]->numeroObjet);
            }
        }
        for(int i=0;i<listPersonnage.length();i++)
        {
            if(listPersonnage[i]->joueur==joueur){listPersonnage[i]->bombe++;}
        }
        while(tailleFlamme!=0)
        {
            compt++;
            if(flammeGauche==true)
            {
                bool flammeOK = true;
                if (!listFlamme.isEmpty())
                {
                    for (int i=0;i<listFlamme.length();i++)
                    {
                        if (listFlamme[i]->position.x==position.x-tailleBlockX*(compt) &&
                                listFlamme[i]->position.y==position.y)
                        {
                            flammeOK=false;
                        }
                    }
                }
                for (int i=0;i<listElementMapInfranchissable.length();i++)
                {
                    if(listElementMapInfranchissable[i]->position.x==position.x-tailleBlockX*(compt) &&
                            listElementMapInfranchissable[i]->position.y==position.y &&
                            listElementMapInfranchissable[i]->arreteFlamme==true)
                    {
                        flammeGauche=false;
                    }
                 }
                for (int i=0;i<listElementMapSensibleFlamme.length();i++)
                {
                    if((listElementMapSensibleFlamme[i]->position.x/tailleBlockX)*tailleBlockX==position.x-tailleBlockX*(compt) &&
                            ((listElementMapSensibleFlamme[i]->position.y+tailleBlockY/2+1)/tailleBlockY)*tailleBlockY==position.y)
                    {
                        emit elementToucheFlamme(listElementMapSensibleFlamme[i]->numeroObjet);

                        if(listElementMapSensibleFlamme[i]->arreteFlamme==true)
                        {
                            flammeGauche=false;
                        }
                    }
                 }
                if(flammeGauche==true && flammeOK == true)
                {
                    Flamme *flamme = new Flamme(this);
                    flamme->numeroFlamme=compteurNombreExplosionBombe;
                    flamme->setTaille(tailleBlockX,tailleBlockY);
                    flamme->setPosition(position.x-(compt*tailleBlockX),position.y);
                    if (tailleFlamme==1){flamme->setImage("../flamme/flamme1/gauche2");flamme->setDirectionFlamme("gauche2");}
                    else flamme->setImage("../flamme/flamme1/gauche1");flamme->setDirectionFlamme("gauche1");
                    flamme->miseAJour();
                    listFlamme.push_back(flamme);
                }

            }

            if(flammeDroite==true)
            {
                bool flammeOK = true;
                if (!listFlamme.isEmpty())
                {
                    for (int i=0;i<listFlamme.length();i++)
                    {
                        if (listFlamme[i]->position.x==position.x+tailleBlockX*(compt) &&
                                listFlamme[i]->position.y==position.y)
                        {
                            flammeOK=false;
                        }
                    }
                }
                for (int i=0;i<listElementMapInfranchissable.length();i++)
                {
                    if(listElementMapInfranchissable[i]->position.x==position.x+tailleBlockX*(compt) &&
                            listElementMapInfranchissable[i]->position.y==position.y &&
                            listElementMapInfranchissable[i]->arreteFlamme==true)
                    {
                        flammeDroite=false;
                    }
                 }
                for (int i=0;i<listElementMapSensibleFlamme.length();i++)
                {
                    if((listElementMapSensibleFlamme[i]->position.x/tailleBlockX)*tailleBlockX==position.x+tailleBlockX*(compt) &&
                            ((listElementMapSensibleFlamme[i]->position.y+tailleBlockY/2+1)/tailleBlockY)*tailleBlockY==position.y)
                    {
                        emit elementToucheFlamme(listElementMapSensibleFlamme[i]->numeroObjet);
                        if(listElementMapSensibleFlamme[i]->arreteFlamme==true)
                        {
                            flammeDroite=false;
                        }
                    }
                 }
                if (flammeDroite==true && flammeOK==true)
                {
                    Flamme *flamme = new Flamme(this);
                    flamme->numeroFlamme=compteurNombreExplosionBombe;
                    flamme->setTaille(tailleBlockX,tailleBlockY);
                    flamme->setPosition(position.x+compt*tailleBlockX,position.y);
                    if (tailleFlamme==1){flamme->setImage("../flamme/flamme1/droite2");flamme->setDirectionFlamme("droite2");}
                    else {flamme->setImage("../flamme/flamme1/droite1");flamme->setDirectionFlamme("droite1");}
                    flamme->miseAJour();
                    listFlamme.push_back(flamme);
                }
            }

            if(flammeHaut==true)
            {
                bool flammeOK = true;
                if (!listFlamme.isEmpty())
                {
                    for (int i=0;i<listFlamme.length();i++)
                    {
                        if (listFlamme[i]->position.x==position.x &&
                                listFlamme[i]->position.y==position.y-tailleBlockY*(compt))
                        {
                            flammeOK=false;
                        }
                    }
                }
                for (int i=0;i<listElementMapInfranchissable.length();i++)
                {
                    if(listElementMapInfranchissable[i]->position.x==position.x &&
                            listElementMapInfranchissable[i]->position.y==position.y-tailleBlockY*(compt)&&
                            listElementMapInfranchissable[i]->arreteFlamme==true)
                    {
                        flammeHaut=false;
                    }
                 }
                for (int i=0;i<listElementMapSensibleFlamme.length();i++)
                {
                    if((listElementMapSensibleFlamme[i]->position.x/tailleBlockX)*tailleBlockX==position.x &&
                            ((listElementMapSensibleFlamme[i]->position.y+tailleBlockY/2+1)/tailleBlockY)*tailleBlockY==position.y-tailleBlockY*compt
                            )
                    {
                        if(listElementMapSensibleFlamme[i]->arreteFlamme==true)
                        {
                            flammeHaut=false;
                        }
                        emit elementToucheFlamme(listElementMapSensibleFlamme[i]->numeroObjet);
                    }
                 }
                if (flammeHaut==true && flammeOK == true)
                {
                    Flamme *flamme = new Flamme(this);
                    flamme->numeroFlamme=compteurNombreExplosionBombe;
                    flamme->setTaille(tailleBlockX,tailleBlockY);
                    flamme->setPosition(position.x,position.y-compt*tailleBlockY);
                    if (tailleFlamme==1){flamme->setImage("../flamme/flamme1/haut2");flamme->setDirectionFlamme("haut2");}
                    else {flamme->setImage("../flamme/flamme1/haut1");flamme->setDirectionFlamme("haut1");}
                    flamme->miseAJour();
                    listFlamme.push_back(flamme);
                }
            }

            if(flammeBas==true)
            {
                bool flammeOK = true;
                if (!listFlamme.isEmpty())
                {
                    for (int i=0;i<listFlamme.length();i++)
                    {
                        if (listFlamme[i]->position.x==position.x &&
                                listFlamme[i]->position.y==position.y+tailleBlockY*(compt))
                        {
                            flammeOK=false;
                        }
                    }
                }
                for (int i=0;i<listElementMapInfranchissable.length();i++)
                {
                    if(listElementMapInfranchissable[i]->position.x==position.x &&
                            listElementMapInfranchissable[i]->position.y==position.y+tailleBlockY*(compt)&&
                            listElementMapInfranchissable[i]->arreteFlamme==true)
                    {
                        flammeBas=false;
                    }
                 }
                for (int i=0;i<listElementMapSensibleFlamme.length();i++)
                {
                    if((listElementMapSensibleFlamme[i]->position.x/tailleBlockX)*tailleBlockX==position.x &&
                            ((listElementMapSensibleFlamme[i]->position.y+tailleBlockY/2+1)/tailleBlockY)*tailleBlockY==position.y+tailleBlockY*compt
                            )
                    {
                        if(listElementMapSensibleFlamme[i]->arreteFlamme==true)
                        {
                            flammeBas=false;
                        }
                        emit elementToucheFlamme(listElementMapSensibleFlamme[i]->numeroObjet);
                    }
                 }
                if (flammeBas==true && flammeOK==true)
                {
                    Flamme *flamme = new Flamme(this);
                    flamme->numeroFlamme=compteurNombreExplosionBombe;
                    flamme->setTaille(tailleBlockX,tailleBlockY);
                    flamme->setPosition(position.x,position.y+compt*tailleBlockY);
                    if (tailleFlamme==1){flamme->setImage("../flamme/flamme1/bas2");flamme->setDirectionFlamme("bas2");}
                    else {flamme->setImage("../flamme/flamme1/bas1");flamme->setDirectionFlamme("bas1");}
                    flamme->miseAJour();
                    listFlamme.push_back(flamme);
                }
            }
            tailleFlamme--;
        }
        int nbJoueurEnVie=0;
        for (int i=0;i<listPersonnage.size();i++)
        {
            if(listPersonnage[i]->enVie==true)
            {
                nbJoueurEnVie++;
            }
        }

        if(nbJoueurEnVie<=1)
        {
            /*for (int i=0;i<listElementMap.size();i++)
            {
                if(listElementMap[i]!=NULL)delete listElementMap[i];
                listElementMap[i]=NULL;
                listElementMap[i]->setImage("../");
                listElementMap[i]->miseAJour();
            }*/
            qDebug() <<"test";

            listBombe.clear();
            listElementMap.clear();
            listElementMapInfranchissable.clear();
            listElementMapSensibleFlamme.clear();
            listFlamme.clear();
            listMur.clear();
            listMurIncassable.clear();
            listSol.clear();
            creationTerrain();
        }
    }

}

void MonMenu::ajouterIntAString(QString *string, int nombre, int nombreCaractere)
{
    int nombreDizaine=0;
    int nombreTemp=nombre;
    while (nombreTemp/10>0)
    {
        nombreDizaine++;
        nombreTemp/=10;
    }
    int compteur=0;
    int division=1;

    for(int i =0;i<nombreCaractere-1;i++)
    {
        division*=10;
    }

    while (nombre<division)
    {
        *string+='0';
        division=division/10;
    }

    division=1;

    while (compteur<nombreDizaine+1)
    {
        nombreTemp=nombre;
        for(int i =0;i<nombreDizaine-compteur;i++)
        {
            division*=10;
        }
        nombre=nombreTemp%division;
        nombreTemp/=division;
        *string+=nombreTemp+0x30;
        compteur++;
        division=1;
    }
}
