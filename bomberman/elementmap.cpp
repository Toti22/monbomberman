#include "elementmap.h"
#include "mainwindow.h"
#include <QTimer>


int ElementMapSensibleFlamme::nbObjet=0;

# ////////////////////// ELEMENT MAP BASIQUE ///////////////////// #

ElementMap::ElementMap(MonMenu* fenetre) : QWidget(fenetre),image(NULL),sensibleFlamme(false),maFenetre(fenetre),pixmapImage(NULL)
{
    arreteFlamme=true;
    taille.x=0;
    taille.y=0;
    image = new QLabel(fenetre);

}

ElementMap::~ElementMap()
{

    if (image!=NULL)
    {
        delete image;
    }
    if (pixmapImage!=NULL)
    {
        delete pixmapImage;
    }
    image=NULL;
    pixmapImage=NULL;

}

void ElementMap::setPosition(int x,int y)
{
    position.x=x;
    position.y=y;
}

void ElementMap::setTaille(int x,int y)
{
    taille.x=x;
    taille.y=y;
}

void ElementMap::setImage(QString chemin)
{
    if(pixmapImage!=NULL){
        delete pixmapImage;
        pixmapImage=NULL;
    }
    pixmapImage = new QPixmap(chemin);
}

void ElementMap::miseAJour()
{
    if (!pixmapImage)
        return;
    if (!image)
        return;
    *pixmapImage = pixmapImage->scaled(taille.x,taille.y,Qt::IgnoreAspectRatio);
    image->setPixmap(*pixmapImage);
    image->setGeometry(position.x,position.y,taille.x,taille.y);
    image->show();
}

# ////////////////////// ELEMENT MAP SENSIBLE FLAMME ! ///////////////////// #

ElementMapSensibleFlamme::ElementMapSensibleFlamme(MonMenu* fenetre) : ElementMap(fenetre),numeroObjet(0)
{
    sensibleFlamme=true;
    nbObjet++;
    numeroObjet=nbObjet;
    connect(fenetre,SIGNAL(elementToucheFlamme(int)),this,SLOT(destructionParFlamme(int)));
}

ElementMapSensibleFlamme::~ElementMapSensibleFlamme()
{

}

# ////////////////////// BOMBE ! ///////////////////// #

Bombe::Bombe(MonMenu* fenetre, int typeTemp,short int flammeTemp): ElementMapSensibleFlamme(fenetre),type(typeTemp),flamme(flammeTemp),
    timer(NULL),timerDeplacement(NULL),compteurAvantBombeExplose(0)
{
    deplacementBas=false;
    deplacementHaut=false;
    deplacementGauche=false;
    deplacementDroite=false;
    arreteFlamme=false;
    franchissable=false;
    setTaille(maFenetre->tailleBlockX,maFenetre->tailleBlockY);
    destructionRetardementVerification=false;
    if (type==1)
    {
        timer = new QTimer();
        timer->setInterval(666);
        timer->start();
        connect(timer,SIGNAL(timeout()),this,SLOT(explosionBombe()));
    }
    if (type==2)
    {
        connect(maFenetre,SIGNAL(signalExplosionBombe()),this,SLOT(explosionBombe()));
    }
    if (type==3)
    {
        timer = new QTimer();
        timer->setInterval(333);
        timer->start();
        connect(timer,SIGNAL(timeout()),this,SLOT(explosionBombe()));
    }
    if (type==4)
    {
        timer = new QTimer();
        timer->setInterval(1000);
        timer->start();
        connect(timer,SIGNAL(timeout()),this,SLOT(explosionBombe()));
    }
    timerDeplacement= new QTimer;
    timerDeplacement->setInterval(30);
    timerDeplacement->start();
    connect(timerDeplacement,SIGNAL(timeout()),this,SLOT(deplacementBombe()));
}

Bombe::~Bombe()
{

    if (timer!=NULL)delete timer;
    position.x=0;
    position.y=0;

}

void Bombe::explosionBombe()
{

    compteurAvantBombeExplose++;
    if (type==1 || type==3 || type==4)
    {
        if (compteurAvantBombeExplose==1)setImage("../bombe/bombe2.png");miseAJour();
        if (compteurAvantBombeExplose==2)setImage("../bombe/bombe3.png");miseAJour();
        if (compteurAvantBombeExplose==3)
        {

            emit bombeExplose(position,type,flamme,joueur);
            deleteLater();
        }
    }
    if (type==2)
    {
        setImage("../bombe/bombeType2_2.png");miseAJour();
        emit bombeExplose(position,1,flamme,joueur);
        setPosition(0,0);
        setVisible(false);
    }
}

void Bombe::destructionParFlamme(int numeroElement)
{
    if(numeroElement == numeroObjet && destructionRetardementVerification==false)
    {
        disconnect(timerDeplacement,SIGNAL(timeout()),this,SLOT(deplacementBombe()));
        position.x=int((position.x/maFenetre->tailleBlockX))*maFenetre->tailleBlockX;
        position.y=int((position.y/maFenetre->tailleBlockY))*maFenetre->tailleBlockY;
        miseAJour();
        destructionRetardementVerification=true;
        if (timer)
            delete timer;
        timer=new QTimer();
        timer->setInterval(10);
        timer->start();
        connect(timer,SIGNAL(timeout()),this,SLOT(destructionRetardement()));
        disconnect((timer,SIGNAL(timeout()),this,SLOT(explosionBombe())));

    }
}

void Bombe::destructionRetardement()
{

    disconnect(timer,SIGNAL(timeout()),this,SLOT(destructionRetardement()));
    if (timer) delete timer;
    timer=NULL;
    emit bombeExplose(position,type,flamme,joueur);
    setPosition(0,0);
    deleteLater();
}

void Bombe::deplacementBombe()
{
    Coord positionAbsolue;
    positionAbsolue.x=int((position.x/maFenetre->tailleBlockX))*maFenetre->tailleBlockX;
    positionAbsolue.y=int((position.y/maFenetre->tailleBlockY))*maFenetre->tailleBlockY;
    if(deplacementBas==true)
    {
        for (int i =0; i<maFenetre->listElementMap.length();i++)
        {
            if (maFenetre->listElementMap[i]->franchissable==false &&
                positionAbsolue.y+maFenetre->tailleBlockY==maFenetre->listElementMap[i]->position.y &&
                positionAbsolue.x==maFenetre->listElementMap[i]->position.x)
            {
                position.x=positionAbsolue.x;
                position.y=positionAbsolue.y;
                deplacementBas=false;
                return;
            }
        }
        position.y=position.y+(maFenetre->resolutionTaille.y/100);
        miseAJour();
    }
    if(deplacementHaut==true)
    {
        for (int i =0; i<maFenetre->listElementMap.length();i++)
        {
            if (maFenetre->listElementMap[i]->franchissable==false &&
                positionAbsolue.y-maFenetre->tailleBlockY==maFenetre->listElementMap[i]->position.y &&
                positionAbsolue.x==maFenetre->listElementMap[i]->position.x)
            {
                if(position.y-(maFenetre->resolutionTaille.y/100)<=positionAbsolue.y)
                {
                    position.x=positionAbsolue.x;
                    position.y=positionAbsolue.y;
                    deplacementHaut=false;
                    return;
                }
            }
        }
        position.y=position.y-(maFenetre->resolutionTaille.y/100);
        miseAJour();
    }
    if(deplacementGauche==true)
    {
        for (int i =0; i<maFenetre->listElementMap.length();i++)
        {
            if (maFenetre->listElementMap[i]->franchissable==false &&
                positionAbsolue.x-maFenetre->tailleBlockX==maFenetre->listElementMap[i]->position.x &&
                positionAbsolue.y==maFenetre->listElementMap[i]->position.y)
            {
                position.x=positionAbsolue.x;
                position.y=positionAbsolue.y;
                deplacementGauche=false;
                return;
            }
        }
        position.x=position.x-(maFenetre->resolutionTaille.x/100);
        miseAJour();
    }
    if(deplacementDroite==true)
    {
        for (int i =0; i<maFenetre->listElementMap.length();i++)
        {
            if (maFenetre->listElementMap[i]->franchissable==false &&
                positionAbsolue.x+maFenetre->tailleBlockX==maFenetre->listElementMap[i]->position.x &&
                positionAbsolue.y==maFenetre->listElementMap[i]->position.y)
            {
                position.x=positionAbsolue.x;
                position.y=positionAbsolue.y;
                deplacementDroite=false;
                return;
            }
        }
        position.x=position.x+(maFenetre->resolutionTaille.x/100);
        miseAJour();
    }

}

# ////////////////////// FLAMME ! ///////////////////// #

Flamme::Flamme(MonMenu* fenetre) : ElementMap(fenetre),compteur(0),timer(NULL)
{
    timer= new QTimer();
    timer->setInterval(50);
    timer->start();
    franchissable=true;
    arreteFlamme=false;

    connect(timer,SIGNAL(timeout()),this,SLOT(annimationFlamme()));
}

Flamme::~Flamme()
{

}

void Flamme::annimationFlamme()
{
    QString chemin;
    chemin="../flamme/";
    if (compteur==0)chemin+="flamme2/";
    if (compteur==1)chemin+="flamme3/";
    if (compteur==2)chemin+="flamme4/";
    if (compteur==3)chemin+="flamme5/";
    if (compteur==4)
    {
        disconnect(timer,SIGNAL(timeout()),this,SLOT(annimationFlamme()));
        if (timer)delete timer;
        deleteLater();
        timer=NULL;
        setPosition(0,0);
    }
    chemin+=directionFlamme;
    chemin+=".png";
    setImage(chemin);
    miseAJour();
    compteur++;
}
