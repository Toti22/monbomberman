#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include <QLabel>
#include <QEvent>
#include <QWidget>
#include "coord.h"
#include "elementmap.h"
#include "bonus.h"

class ElementMap;
class MonMenu;
class QKeyEvent;

class Personnage: public ElementMapSensibleFlamme
{
    friend class Bonus;
    friend class MonMenu;
    Q_OBJECT
public:
    Personnage(MonMenu*);
    Personnage(MonMenu*, int);// choix du joueur (joueur1, joueur2 etc...)
    Personnage(MonMenu*,int,struct Coord);//choix joueur + ses coord
    Personnage(MonMenu*,int,int,int,int,struct Coord);// choix joueur(le premier int) + ses caractéristique basique (bombe,flamme,vitesse)
    virtual ~Personnage();
    static int nbToucheUtile;
private:

    // ####### TOUCHE ###### //

    int toucheBas;
    int toucheHaut;
    int toucheGauche;
    int toucheDroite;
    int touchePoserBombe;
    int toucheAction1;

    // ####### TOUCHE ###### //

    short int bombe;
    short int flamme;
    short int vitesse;
    bool lancer;
    bool pousser;
    bool enVie;
    bool jouable;
    int typeBombe;
    short int joueur;

    int compteur;
    /*int tailleBlockXPerso;
    int tailleBlockYPerso;*/

    bool deplacementBas;
    bool deplacementHaut;
    bool deplacementGauche;
    bool deplacementDroite;

    QTimer *timer;

    //QKeyEvent keyEvent;

    void apparencePersonnage(MonMenu*, QString chemin);
    virtual void destructionParFlamme(int);
private slots:
        void deplacerJoueur();
        void poserBombe(Personnage* joueur1);
};

#endif // PERSONNAGE_H
