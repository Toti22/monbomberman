#include "boutontouche.h"
#include "mainwindow.h"
#include "QPushButton"
#include "QAbstractButton"
#include "qabstractbutton.h"
#include "qabstractitemview.h"
#include "qbuttongroup.h"
#include "qevent.h"
#include "qpainter.h"
#include "qapplication.h"
#include "qstyle.h"
#include "qaction.h"
#include "qaccessible.h"
#include <QEvent>
#include <QKeyEvent>
#include <QMainWindow>
#include <QWidget>

QString intToString(int touche)
{
    QString texte="";
    int nombreDizaine;
    int toucheTemp=touche;
    while (toucheTemp>9)
    {
        nombreDizaine++;
        toucheTemp /=10;
    }
    toucheTemp=touche;
    while(touche>9)
    {
        toucheTemp=touche%10;
        toucheTemp+=0x30;
        texte= texte + toucheTemp;
        touche/=10;
    }
    touche = touche + 0x30;
    texte= texte + touche;

    for(int i =0;i<texte.length()/2;i++)
    {
        QString caractTemp="";
        caractTemp[0]=texte[i];
        texte[i]=texte[texte.length()-i-1];
        texte[texte.length()-i-1]=caractTemp[0];
    }
    return texte;
}

int BoutonTouche::nbBouton = 0;

BoutonTouche::BoutonTouche(MonMenu *fenetre,int joueurTemp): QPushButton()
{
    maFenetre=fenetre;
    joueur=joueurTemp;
    numBouton=nbBouton;
    nbBouton++;

}

void BoutonTouche::mouseReleaseEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)click();
}

void BoutonTouche::keyPressEvent(QKeyEvent *key)
{
    QString texte=intToString(key->key());
    int touche = key->key();
    emit toucheChoisie(key->key(),joueur,numBouton);
    if (touche==65)setText("A");
    else if (touche==66)setText("B");
    else if (touche==67)setText("C");
    else if (touche==68)setText("D");
    else if (touche==69)setText("E");
    else if (touche==70)setText("F");
    else if (touche==71)setText("G");
    else if (touche==72)setText("H");
    else if (touche==73)setText("I");
    else if (touche==74)setText("J");
    else if (touche==75)setText("K");
    else if (touche==76)setText("L");
    else if (touche==77)setText("M");
    else if (touche==78)setText("N");
    else if (touche==79)setText("O");
    else if (touche==80)setText("P");
    else if (touche==81)setText("Q");
    else if (touche==82)setText("R");
    else if (touche==83)setText("S");
    else if (touche==84)setText("T");
    else if (touche==85)setText("U");
    else if (touche==86)setText("V");
    else if (touche==87)setText("W");
    else if (touche==88)setText("X");
    else if (touche==89)setText("Y");
    else if (touche==90)setText("Z");
    else if (touche==16777235)setText("Fleche Haut");
    else if (touche==16777234)setText("Fleche Gauche");
    else if (touche==16777237)setText("Fleche Bas");
    else if (touche==16777236)setText("Fleche Droite");
    else if (touche==16777248)setText("Shift");
    else if (touche==16777249)setText("Ctrl");
    else if (touche==16777220)setText("Entrée");
    else if (touche==44)setText(",");
    else if (touche==59)setText(";");
    else if (touche==58)setText(":");
    else if (touche==33)setText("!");
    else if (touche==63)setText("?");
    else if (touche==46)setText(".");
    else if (touche==47)setText("/");
    else if (touche==32)setText("Espace");
    else if (touche==167)setText("§");
    else setText(texte);

}

bool BoutonTouche::event(QEvent *eventTemp)
{
    return QWidget::event(eventTemp);
}

void BoutonTouche::click()
{
    setText("");
}
