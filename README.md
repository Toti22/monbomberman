# MonBomberMan

## Contexte du projet

J'ai réalisé cette application lors ma deuxième année d'étude à l'aide du c++ et du framework Qt. 
Ce projet représente mes premiers pas dans le monde de la programmation.
La propreté du code est loin de mes nouveaux standards.

Dès le début de mes études en programmation, j'ai commencé à réaliser des projets personnels sur mon temps libre. Ce Bomberman est mon premier projet personnel que je trouve digne d'être partagé.

----> https://youtu.be/gaiN9I0DHzQ <---- Une vidéo de démonstration du fonctionnement du jeu en multijoueur en réseau local est disponible ici

## Fonctionnalités

- Jouable jusque 2 joueurs sur un même PC.
- Deux options de résolution possibles.
- Personnalisation des touches.
- Jouable jusque 4 joueurs sur un réseau local

## Commentaires techniques

- Choix d'utiliser le protocole TCP pour simplifier la gestion des paquets reçu/non reçu. (Le programme par du principe que tous les paquets arriveront à destination).
- Quelques choix techniques en terme de POO intéressant. Exemple, la méthode virtuel : *virtual void destructionParFlamme(int)=0;*. 
	Les éléments de la présents sur le terrain de jeu héritent de cette méthode. Ainsi les éléments comme les murs destructibles, les joueurs, les bonus, ou encore les bombes, peuvent redéfinir cette méthode et adopter un comportement adéquat :
	* Si une flamme touche un joueur, celui-ci meurt.
	* Si une flamme touche un mur destructible ou un bonus, celui-ci disparait.
	* si une flamme touche une bombe, celle-ci explose immédiatement.